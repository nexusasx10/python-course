import sys
import logging
from collections import defaultdict
from os import path as paths, walk, makedirs
from shutil import copyfile
from argparse import ArgumentParser

from lib.man import ManDocumentConstructor, ManDocument
from lib.parser import Parser, TEXT_REGEX, BACKSLASH_REGEX, WHITESPACE_REGEX, \
    NEWLINE_REGEX, RegexReader, ManMarcosReader, ParsingError, UNKNOWN_REGEX, \
    MINUS_REGEX, LINK_REGEX


class Analyzer:

    def __init__(self, parser):
        self._parser = parser

    def analyze(self, fd):
        document = ManDocument()
        constructor = ManDocumentConstructor()
        tokens = []
        try:
            for i, line_ in enumerate(fd):
                try:
                    tokens.extend(self._parser.parse(line_, i))
                except ParsingError as exc:
                    logging.error(
                        '{}:{}:{}: unknown format'.format(
                            fd.name, exc.line, exc.char
                        )
                    )
                    return
        except (OSError, UnicodeDecodeError) as exc:
            logging.error(exc)
        pointer = 0
        while pointer < len(tokens):
            token = tokens[pointer]
            for type_ in token.types:
                if type_ in constructor.handlers:
                    constructor.handlers[type_](pointer, tokens, document)
            pointer += 1
        return document


def parse_args(args=None):
    parser = ArgumentParser(description='Convert man documentation to HTML.')
    parser.add_argument(
        '-l', '--log',
        help='write logs to file',
        metavar='filename',
        nargs='?',
        const='man2html.log'
    )
    parser.add_argument(
        '-p', '--paths',
        help='paths to man files or directories',
        nargs='+',
        metavar='path',
        default=['.']
    )
    return parser.parse_args(args)


def logging_config(args):
    handlers = [logging.StreamHandler(sys.stdout)]
    if args.log:
        handlers.append(logging.FileHandler(args.log))
    logging.basicConfig(
        handlers=handlers,
        level=logging.INFO,
        format='[%(asctime)s.%(msecs)d][%(levelname)s] %(message)s',
        datefmt='%d.%m.%Y %H:%M:%S'
    )


def main():
    args = parse_args()
    logging_config(args)
    logging.info('app running')
    parser = Parser()
    parser.add_reader(RegexReader(LINK_REGEX, 'link'), 0)
    parser.add_reader(RegexReader(MINUS_REGEX, 'minus'), 0)
    parser.add_reader(RegexReader(UNKNOWN_REGEX, 'unknown'), 0)
    parser.add_reader(RegexReader(WHITESPACE_REGEX, 'whitespace'), 1)
    parser.add_reader(RegexReader(NEWLINE_REGEX, 'newline'), 0)
    parser.add_reader(RegexReader(BACKSLASH_REGEX, 'text'), 1)
    parser.add_reader(ManMarcosReader(), 0)
    parser.add_reader(RegexReader(TEXT_REGEX, 'text'), 1)
    analyzer = Analyzer(parser)
    file_paths = []
    for path in args.paths:
        if paths.isdir(path):
            for root, _, files in walk(path):
                files = map(lambda file: paths.join(root, file), files)
                file_paths.extend(files)
        elif paths.isfile(path):
            file_paths.append(path)
    visited = defaultdict(dict)
    for file_path in file_paths:
        try:
            with open(file_path, encoding='utf-8') as source_fd:
                document = analyzer.analyze(source_fd)
            target_dir = paths.join(
                paths.dirname(file_path),
                'man2html'
            )
            target_path = paths.join(
                target_dir,
                paths.splitext(paths.basename(file_path))[0] + '.html'
            )
            makedirs(target_dir, exist_ok=True)
            visited[target_dir][document.title] = target_path
            with open(target_path, mode='w', encoding='utf-8') as target_fd:
                target_fd.write(document.to_html())
            logging.info(file_path + ' handled')
        except OSError as exc:
            logging.error(exc)
    for target_dir in visited:
        try:
            copyfile(
                paths.join('templates', 'styles.css'),
                paths.join(target_dir, 'styles.css')
            )
        except OSError as exc:
            logging.error(exc)
        path = paths.join('templates', 'index_template.html')
        content = ''
        if len(visited[target_dir]) < 2:
            continue
        for name in visited[target_dir]:
            content += '<a href="{}">{}</a><br>'.format(
                paths.basename(visited[target_dir][name]), name
            )
        index = paths.split(target_dir)[1]
        try:
            with open(path, encoding='utf-8') as fd:
                content = fd.read().format(index=index, content=content)
            path = paths.join(target_dir, '__index.html')
            with open(path, mode='w', encoding='utf-8') as fd:
                fd.write(content)
        except OSError as exc:
            logging.error(exc)
    logging.info('app terminated')


if __name__ == '__main__':
    main()
