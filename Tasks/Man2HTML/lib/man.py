from os import path as paths


class ManDocumentConstructor:

    def __init__(self):
        self.font_mode = 'R'
        self.font_first = True
        self.prev_font = None
        self.style = []
        self.comment = False
        self.section = None
        self.indent = 1
        self.newline = True
        self.handlers = {
            'whitespace': self.whitespace_handler,
            'text': self.text_handler,
            'newline': self.newline_handler,
            'minus': self.minus_handler,
            'link': self.link_handler,
            '.\\': self.comment_handler,
            '."': self.comment_handler,
            '.\\"': self.comment_handler,
            '."\\': self.comment_handler,
            '.TP': lambda *args: None,
            '.TH': self.th_handler,
            '.SH': self.sh_handler,
            '.PD': self.pd_handler,
            '.LP': self.pp_handler,
            '.PP': self.pp_handler,
            '.P': self.pp_handler,
            '.B': self.font_handler,
            '.BI': self.font_handler,
            '.br': self.font_handler,
            '.BR': self.font_handler,
            '.I': self.font_handler,
            '.IB': self.font_handler,
            '.IR': self.font_handler,
            '\\fB': self.font_handler,
            '\\fI': self.font_handler,
            '\\fR': self.font_handler,
            '\\fP': self.font_handler,
            '\\c': lambda *args: None,
        }

    def whitespace_handler(self, pointer, sequence, document):
        if not self.comment and self.section:
            document.sections[self.section].content += sequence[pointer].text

    def text_handler(self, pointer, sequence, document):
        if self.newline and document.sections and self.section:
            document.sections[self.section].content += '    ' * self.indent
            self.newline = False
        if not self.comment and self.section:
            document.sections[self.section].content += sequence[pointer].text

    def newline_handler(self, pointer, sequence, document):
        self.font_first = True
        self.font_mode = 'R'
        self.newline = True
        if self.section and not self.comment:
            document.sections[self.section].content += '<br>'
        self.comment = False

    def minus_handler(self, pointer, sequence, document):
        if self.section:
            document.sections[self.section].content += '-'

    def link_handler(self, pointer, sequence, document):
        if self.section:
            text = sequence[pointer].text.replace('\-', '-')
            document.sections[
                self.section
            ].content += '<a href="{}">{}</a>'.format(text, text)

    def comment_handler(self, pointer, sequence, document):
        self.comment = True

    def th_handler(self, pointer, sequence, document):
        pointer += 1
        document.title, pointer = step(sequence, pointer)
        document.chapter_number, pointer = step(sequence, pointer)
        document.center_footer, pointer = step(sequence, pointer)
        document.left_footer, pointer = step(sequence, pointer)
        document.center_header, pointer = step(sequence, pointer)
        self.comment = True

    def sh_handler(self, pointer, sequence, document):
        pointer += 1
        name, pointer = step(sequence, pointer)
        document.sections.append(Section(name))
        self.section = len(document.sections) - 1
        self.comment = True

    def pd_handler(self, pointer, sequence, document):
        pointer += 1
        self.indent = 4

    def pp_handler(self, pointer, sequence, document):
        self.indent = 1
        self.comment = True
        if self.section:
            document.sections[self.section].content += '<br>'

    def font_handler(self, pointer, sequence, document):
        self.font_mode = sequence[1:][pointer]
        self.font_first = True


class ManDocument:

    def __init__(self):
        self.title = ''
        self.chapter_number = ''
        self.center_footer = ''
        self.left_footer = ''
        self.center_header = ''
        self.sections = []

    def to_html(self):
        path = paths.join('templates', 'man_document_template.html')
        with open(path, encoding='utf-8') as fd:
            return fd.read().format(
                title=self.title,
                center_footer=self.center_footer,
                left_footer=self.left_footer,
                center_header=self.center_header,
                sections=''.join(map(lambda s: s.to_html(), self.sections)),
                chapter_number=self.chapter_number
            )


class Section:
    def __init__(self, name):
        self.name = name
        self.content = ''

    def to_html(self):
        with open('templates/section_template.html', encoding='utf-8') as fd:
            return fd.read().format(
                name=self.name,
                content=self.content
            )


def step(sequence, pointer):
    token = sequence[pointer]
    pointer += 1
    while token['whitespace']:
        token = sequence[pointer]
        pointer += 1
    if not token['newline']:
        return quote_strip(token.text), pointer
    else:
        return '', pointer


def quote_strip(text):
    if text.startswith('"'):
        text = text[1:]
    if text.endswith('"'):
        text = text[:-1]
    return text
