import re

from lib.man import ManDocumentConstructor


class ParsingError(Exception):

    def __init__(self, line, char, *args):
        super().__init__(*args)
        self.line = line
        self.char = char


class Parser:

    def __init__(self):
        self._readers = {}

    def add_reader(self, reader, priority):
        self._readers[reader] = priority

    def parse(self, line, line_number):
        pointer = 0
        while pointer < len(line):
            token = None
            last_priority = float('+inf')
            next_pointer = 0
            for reader in self._readers:
                maybe_token = reader.try_read(line, line_number, pointer)
                if maybe_token and (
                        not token
                        or self._readers[reader] < last_priority
                ):
                    token = maybe_token
                    next_pointer = pointer + len(token)
                last_priority = self._readers[reader]
            if token:
                yield token
                pointer = next_pointer
            else:
                raise ParsingError(line_number, pointer)


class Token:

    def __init__(self, types, text, line, start):
        self.line = line
        self.start = start
        self.types = set(types.split())
        self.text = text

    def __getitem__(self, item):
        return item in self.types

    def __len__(self):
        return len(self.text)

    def __str__(self):
        return '<{} : {} : {} : {}>'.format(
            ', '.join(self.types),
            self.line,
            self.start,
            self.text.replace('\n', '\\n')
        )


TEXT_REGEX = r'("[^"]+")|([^ \n\t\\]+)'
BACKSLASH_REGEX = r'\\'
WHITESPACE_REGEX = '\s+'
NEWLINE_REGEX = '\n'
UNKNOWN_REGEX = r'\\[,/c%]'
MINUS_REGEX = r'\\-'
LINK_REGEX = r'http\S+'


class RegexReader:

    def __init__(self, pattern, types):
        self.regex = re.compile(pattern)
        self.types = types

    def try_read(self, text, line, start):
        match = self.regex.match(text, pos=start)
        if match:
            return Token(self.types, match.group(0), line, start)


class RegexBeginReader(RegexReader):

    def try_read(self, text, line, start):
        if start > 0:
            return
        return super().try_read(text, line, start)


class ManMarcosReader:

    def try_read(self, text, line, start):
        if text[start] == '.':
            types_ = 'macros start'
        elif text[start:start + 2] == '\\f':
            types_ = 'macros inline'
        else:
            return
        if text[start:start + 4] in ManDocumentConstructor().handlers:
            types_ += ' ' + text[start:start + 4]
            return Token(types_, text[start:start + 4], line, start)
        elif text[start:start + 3] in ManDocumentConstructor().handlers:
            types_ += ' ' + text[start:start + 3]
            return Token(types_, text[start:start + 3], line, start)
        elif text[start:start + 2] in ManDocumentConstructor().handlers:
            types_ += ' ' + text[start:start + 2]
            return Token(types_, text[start:start + 2], line, start)
        elif text[start] in ManDocumentConstructor().handlers:
            types_ += ' ' + text[start]
            return Token(types_, text[start], line, start)
