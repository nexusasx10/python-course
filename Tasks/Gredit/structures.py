"""Библиотека основных программных классов."""
from math import ceil

from PIL import Image

from basics import Point, Color, FileOpenError
from instruments import Brush
from utils import Colors


class ImageInfo:
    """Используется для получения и хранения информации об изображении."""

    def __init__(self, file_=None, width=None, height=None,
                 h_res=None, v_res=None):
        """Получает изображение и информацию о нём из переданного файла."""
        if file_ is not None:
            ImageInfo._check_signature(file_)
            ImageInfo._check_compatibility(file_)
            file_.seek(10)
            offset = int.from_bytes(file_.read(4), byteorder='little')
            self._get_image_info(file_)
            self._get_image(file_, offset)
        else:
            self.width = width
            self.height = height
            self.h_res = h_res
            self.v_res = v_res

    @staticmethod
    def _check_signature(file_):
        """Проверка сигнатуры файла на корректность."""
        file_.seek(0)
        type_ = file_.read(2)
        size = int.from_bytes(file_.read(4), byteorder='little')
        actual_size = file_.seek(0, 2)
        if type_ != b'BM' or size != actual_size:
            raise FileOpenError('Image is corrupted', False)

    @staticmethod
    def _check_compatibility(file_):
        """Проверка файла на совместимость."""
        file_.seek(26)
        planes = int.from_bytes(file_.read(2), byteorder='little')
        depth = int.from_bytes(file_.read(2), byteorder='little')
        compressed = int.from_bytes(file_.read(4), byteorder='little')
        if planes != 1 or depth != 24 or compressed != 0:
            raise FileOpenError('Unsupported format', False)

    def _get_image_info(self, file_):
        """Получает информацию о изображении из файла."""
        file_.seek(18)
        self.width = int.from_bytes(file_.read(4), byteorder='little')
        self.height = int.from_bytes(file_.read(4), byteorder='little')
        if self.width == 0 or self.height == 0:
            raise FileOpenError('Image is empty', False)
        file_.seek(38)
        self.h_res = int.from_bytes(file_.read(4), byteorder='little')
        self.v_res = int.from_bytes(file_.read(4), byteorder='little')

    def _get_image(self, file_, offset):
        """Получает изображение из файла."""
        file_.seek(offset)
        self.image = [[None] * self.height for _ in range(self.width)]
        extra = (self.width * 3) % 4
        empty_pixels = 4 - extra if extra != 0 else extra
        for y in range(self.height):
            for x in range(self.width):
                pixel = file_.read(1), file_.read(1), file_.read(1)
                self.image[x][self.height - y - 1] = (
                    int.from_bytes(pixel[2], byteorder='little'),
                    int.from_bytes(pixel[1], byteorder='little'),
                    int.from_bytes(pixel[0], byteorder='little')
                )
            file_.read(empty_pixels)

    def pack(self, file_, image):
        """Упаковывает изображение и информацию о нём в файл."""
        self._pack_signature(file_)
        self._pack_image_info(file_)
        self._pack_image(file_, image)

    def _pack_signature(self, file_):
        type_ = b'BM'
        extra = (self.width * 3) % 4
        empty_pixels = 4 - extra if extra != 0 else extra
        image_size = (self.width * 3 + empty_pixels) * self.height
        file_size = 54 + image_size
        offset = 54
        file_.write(type_)
        file_.write(file_size.to_bytes(byteorder='little', length=4))
        file_.write(bytes(b'\x00\x00\x00\x00'))
        file_.write(offset.to_bytes(byteorder='little', length=4))

    def _pack_image_info(self, file_):
        extra = (self.width * 3) % 4
        empty_pixels = 4 - extra if extra != 0 else extra
        image_size = (self.width * 3 + empty_pixels) * self.height
        header_len = 40
        planes = 1
        depth = 24
        compressed = 0
        used_colors = 0
        important_colors = 0
        file_.write(header_len.to_bytes(byteorder='little', length=4))
        file_.write(self.width.to_bytes(byteorder='little', length=4))
        file_.write(self.height.to_bytes(byteorder='little', length=4))
        file_.write(planes.to_bytes(byteorder='little', length=2))
        file_.write(depth.to_bytes(byteorder='little', length=2))
        file_.write(compressed.to_bytes(byteorder='little', length=4))
        file_.write(image_size.to_bytes(byteorder='little', length=4))
        file_.write(self.h_res.to_bytes(byteorder='little', length=4))
        file_.write(self.v_res.to_bytes(byteorder='little', length=4))
        file_.write(used_colors.to_bytes(byteorder='little', length=4))
        file_.write(important_colors.to_bytes(byteorder='little', length=4))

    def _pack_image(self, file_, image):
        """Упаковывает изображение в файл."""
        extra = (self.width * 3) % 4
        empty_pixels = 4 - extra if extra != 0 else extra
        for y in range(self.height):
            for x in range(self.width):
                pixel = image[(x, self.height - y - 1)]
                file_.write(pixel.blue.to_bytes(byteorder='little', length=1))
                file_.write(pixel.green.to_bytes(byteorder='little', length=1))
                file_.write(pixel.red.to_bytes(byteorder='little', length=1))
            for _ in range(empty_pixels):
                file_.write(b'\x00')


class WorkingCanvas:
    """Используется для хранения и редактирования изображения."""

    def __init__(self, width=None, height=None, background_color=None,
                 image_info=None):
        if image_info is not None:
            self.width = image_info.width
            self.height = image_info.height
            self.background_color = Colors.white
        else:
            self.width = width
            self.height = height
            self.background_color = background_color

        self.block_size = 200
        self.grid_width = ceil(self.width / self.block_size)
        self.grid_height = ceil(self.height / self.block_size)
        width_mod = self.width % self.block_size
        height_mod = self.height % self.block_size
        self.image = [[None] * self.grid_width for _ in range(
            self.grid_height
        )]

        for j in range(self.grid_width):
            for i in range(self.grid_height):
                cell_width = width_mod if j == self.grid_width - 1 and \
                    width_mod > 0 else self.block_size
                cell_height = height_mod if i == self.grid_height - 1 and \
                    height_mod > 0 else self.block_size
                if image_info is not None:
                    cell = Image.new('RGB', (cell_width, cell_height))
                    for y in range(cell_height):
                        for x in range(cell_width):
                            cell.putpixel(
                                (x, y),
                                image_info.image[
                                    j * self.block_size + x
                                ][
                                    i * self.block_size + y
                                ]
                            )
                else:
                    cell = Image.new('RGB', (cell_width, cell_height),
                                     background_color.to_tuple())
                self.image[i][j] = cell

    def block_coordinates(self, i, j):
        """Возвращает координаты блока."""
        return Point(j * self.block_size, i * self.block_size)

    def __getitem__(self, position):
        if isinstance(position, (tuple, list)):
            position = Point(*position)
        i = position.y // self.block_size
        j = position.x // self.block_size
        x = position.x % self.block_size
        y = position.y % self.block_size
        return Color(*self.image[i][j].getpixel((x, y)))

    def __setitem__(self, position, value):
        if isinstance(position, (tuple, list)):
            position = Point(*position)
        if isinstance(value, Color):
            value = value.to_tuple()
        i = position.y // self.block_size
        j = position.x // self.block_size
        x = position.x % self.block_size
        y = position.y % self.block_size
        self.image[i][j].putpixel((x, y), value)


class WorkSpace:

    def __init__(self, width=None, height=None, resolution=None,
                 background=None, file_=None):
        if file_ is not None:
            self.image_info = ImageInfo(file_)
            self.file_path = file_.name
            self.canvas = WorkingCanvas(image_info=self.image_info)
        else:
            self.image_info = ImageInfo(
                width=width,
                height=height,
                h_res=resolution,
                v_res=resolution
            )
            self.file_path = None
            self.canvas = WorkingCanvas(
                width=width,
                height=height,
                background_color=background
            )
        self.brush = Brush(
            canvas=self.canvas,
            size=1,
            color=Colors.black,
            type_='brush',
            form='circle'
        )
        self.grid = [[None] * self.canvas.grid_width for _ in range(
            self.canvas.grid_height
        )]
        self.operation_queue = []
        self.integral_operation = []
        self.undo_stack = []
        self.redo_stack = []
