#!/usr/bin/python3
"""версия редактора с графическим интерфейсом."""
from threading import Thread
from os import name as os_name
from argparse import ArgumentParser
from logging import error, info, basicConfig, INFO
from time import sleep

from tkinter import Canvas as DisplayCanvas
from tkinter import Label, Entry, Scale, Button, Frame, OptionMenu
from tkinter import PhotoImage, StringVar
from tkinter import Tk, Toplevel, messagebox, colorchooser, filedialog
from PIL import ImageTk

from basics import Point, Color, FileOpenError
from structures import WorkSpace
from instruments import Filter, Tools


class GUI:
    """Графический интерфейс приложения."""

    def __init__(self, height, width, path_=None):
        self.workspace = None
        self.root = Tk()
        self.root.geometry('{}x{}+{}+{}'.format(width, height, 50, 50))
        self.root.title('Gredit BMP')
        if os_name == 'nt':
            self.root.iconbitmap(bitmap='images/Gredit.ico')

        self.root.protocol('WM_DELETE_WINDOW', self.destroy)
        self.root.bind('<Configure>', self.size_recalculate)
        self.root.bind('<Control-o>', self.open_file)
        self.root.bind('<Control-n>', self.create_file_dialog)
        self.root.bind('<Control-s>', self.save_file)
        self.root.bind('<Control-z>', self.undo_operation)
        self.root.bind('<Control-y>', self.redo_operation)

        self.images = {
            'open_file': PhotoImage(file='images/open.gif'),
            'create_file': PhotoImage(file='images/create.gif'),
            'save_file': PhotoImage(file='images/save.gif'),
            'save_file_as': PhotoImage(file='images/save_as.gif'),
            'undo': PhotoImage(file='images/undo.gif'),
            'redo': PhotoImage(file='images/redo.gif'),
            'close': PhotoImage(file='images/close.gif'),
            'help': PhotoImage(file='images/help.gif'),
            'swap': PhotoImage(file='images/swap.gif'),
            'brush': PhotoImage(file='images/brush.gif'),
            'eraser': PhotoImage(file='images/erase.gif'),
            'spray':  PhotoImage(file='images/spray.gif'),
            'grayscale': PhotoImage(file='images/grayscale.gif'),
            'negative': PhotoImage(file='images/negative.gif'),
            'sepia': PhotoImage(file='images/sepia.gif'),
            'brightness': PhotoImage(file='images/brightness.gif'),
            'blur': PhotoImage(file='images/blur.gif'),
            'clean': PhotoImage(file='images/clean.gif')
        }

        # Creating of Toolbar.
        self.toolbar = Frame(master=self.root)
        self.toolbar.pack(side='top', fill='x')

        self.file_group = Frame(master=self.toolbar)
        self.file_group.pack(side='left')

        self.open_file_button = Button(
            self.file_group,
            width=40,
            height=40,
            relief='flat',
            borderwidth=1,
            image=self.images['open_file'],
            text='Open',
            compound='top',
            command=self.open_file
        )
        self.open_file_button.pack(side='left')

        self.create_file_button = Button(
            self.file_group,
            width=40,
            height=40,
            relief='flat',
            borderwidth=1,
            image=self.images['create_file'],
            text='Create',
            compound='top',
            command=self.create_file_dialog
        )
        self.create_file_button.pack(side='left')

        self.save_file_button = Button(
            self.file_group,
            width=40,
            height=40,
            relief='flat',
            borderwidth=1,
            image=self.images['save_file'],
            text='Save',
            compound='top',
            command=self.save_file
        )
        self.save_file_button.pack(side='left')

        self.save_file_as_button = Button(
            self.file_group,
            width=40,
            height=40,
            relief='flat',
            borderwidth=1,
            image=self.images['save_file_as'],
            text='Save as',
            compound='top',
            command=self.save_file_as
        )
        self.save_file_as_button.pack(side='left')

        self.close_button = Button(
            self.file_group,
            width=40,
            height=40,
            relief='flat',
            borderwidth=1,
            image=self.images['close'],
            text='Close',
            compound='top',
            command=self.close
        )
        self.close_button.pack(side='left')

        gap = Frame(self.toolbar, width=1, bg='#aaa')
        gap.pack(fill='y', side='left')

        self.history_group = Frame(self.toolbar)
        self.history_group.pack(side='left')

        self.undo_button = Button(
            self.history_group,
            width=40,
            height=40,
            relief='flat',
            borderwidth=1,
            image=self.images['undo'],
            text='Undo',
            compound='top',
            command=self.undo_operation
        )
        self.undo_button.pack(side='left')

        self.redo_button = Button(
            self.history_group,
            width=40,
            height=40,
            relief='flat',
            borderwidth=1,
            image=self.images['redo'],
            text='Redo',
            compound='top',
            command=self.redo_operation
        )
        self.redo_button.pack(side='left')

        Frame(
            self.toolbar,
            width=1,
            bg='#aaa'
        ).pack(
            fill='y',
            side='left'
        )

        self.help_group = Frame(self.toolbar)
        self.help_group.pack(side='right')

        self.help_button = Button(
            self.help_group,
            width=40,
            height=40,
            relief='flat',
            borderwidth=1,
            image=self.images['help'],
            text='Help',
            compound='top',
            command=self.help
        )
        self.help_button.pack(side='right')

        Frame(
            self.root,
            height=1,
            bg='#aaa'
        ).pack(
            fill='x',
            side='top'
        )

        # Creating of Status bar
        self.status_bar = Frame(self.root, relief='sunken', bd=1)
        self.status_bar.pack(side='bottom', fill='x')

        self.progress_bar = Frame(
            self.status_bar,
            width=200,
            bg='#ccc'
        )
        self.progress_bar.pack(side='right', fill='y')

        self.progress_indicator = Frame(
            self.progress_bar,
            width=0,
            height=20,
            bg='#ccc'
        )
        self.progress_indicator.place(x=0, y=0)

        self.status_indicator = Label(
            self.status_bar,
            anchor='w'
        )
        self.status_indicator.pack(fill='x')

        # Creating of Control panel
        self.control_panel = Frame(self.root)
        self.control_panel.pack(side='left', fill='y')

        self.brush_change_group = Frame(
            master=self.control_panel,
            padx=10,
            pady=10
        )
        self.brush_change_group.pack(side='top')

        caption = Label(self.brush_change_group, text='Color')
        caption.pack(side='top', fill='x')

        self.color_changers = Frame(
            self.brush_change_group,
            width=60,
            height=60
        )
        self.color_changers.pack(side='top')

        container = Frame(
            self.color_changers,
            width=40,
            height=40
        )
        container.pack_propagate(0)
        container.place(x=20, y=20)

        self.background_color_changer = Button(
            container,
            relief='raise',
            borderwidth=1,
            command=self.change_background_color
        )
        self.background_color_changer.pack(expand=1, fill='both')

        container = Frame(
            self.color_changers,
            width=40,
            height=40
        )
        container.pack_propagate(0)
        container.place(x=0, y=0)

        self.brush_color_changer = Button(
            container,
            relief='raise',
            borderwidth=1,
            command=self.change_brush_color
        )
        self.brush_color_changer.pack(expand=1, fill='both')

        self.color_changers_swap = Button(
            self.color_changers,
            width=10,
            height=10,
            relief='raise',
            borderwidth=1,
            image=self.images['swap'],
            command=self.swap_color_changers
        )
        self.color_changers_swap.place(x=0, y=45)

        caption = Label(self.brush_change_group, text='Size')
        caption.pack(side='top', fill='x')

        self.brush_size_changer = Scale(
            self.brush_change_group, from_=1, orient='horizontal',
            command=self.change_brush_size
        )
        self.brush_size_changer.pack(side='top')

        caption = Label(self.brush_change_group, text='Form')
        caption.pack(side='top', fill='x')

        self.variable = StringVar(self.root)
        self.variable.set('')
        self.brush_form_changer = OptionMenu(
            master=self.brush_change_group,
            variable=self.variable,
            value=None
        )
        self.brush_form_changer.pack()

        caption = Label(self.brush_change_group, text='Type')
        caption.pack(side='top', fill='x')

        self.brush_type = Frame(self.brush_change_group)
        self.brush_type.pack()

        self.brush_button = Button(
            self.brush_type,
            width=40,
            height=40,
            relief='flat',
            borderwidth=1,
            image=self.images['brush'],
            text='Brush',
            compound='top',
            command=lambda: self.change_brush_type('brush')
        )
        self.brush_button.pack(side='left')

        self.eraser_button = Button(
            self.brush_type,
            width=40,
            height=40,
            relief='flat',
            borderwidth=1,
            image=self.images['eraser'],
            text='Eraser',
            compound='top',
            command=lambda: self.change_brush_type('eraser')
        )
        self.eraser_button.pack(side='left')

        self.spray_button = Button(
            self.brush_type,
            width=40,
            height=40,
            relief='flat',
            borderwidth=1,
            image=self.images['spray'],
            text='Spray',
            compound='top',
            command=lambda: self.change_brush_type('spray')
        )
        self.spray_button.pack(side='left')

        Frame(
            self.control_panel,
            height=1,
            bg='#aaa'
        ).pack(
            fill='x',
            side='top'
        )

        caption = Label(self.control_panel, text='Zoom')
        caption.pack(side='top', fill='x')

        self.zoom_changer = Scale(
            self.control_panel,
            from_=1,
            to=5,
            orient='horizontal',
            command=self.zoom
        )
        self.zoom_changer.pack(side='top')

        Frame(
            self.control_panel,
            height=1,
            bg='#aaa'
        ).pack(
            fill='x',
            side='top'
        )

        self.filter_group = Frame(
            self.control_panel,
            padx=10,
            pady=10
        )
        self.filter_group.pack(side='top')

        Label(
            self.filter_group,
            text='Filters'
        ).pack(
            side='top',
            fill='x'
        )

        self.grayscale_filter_button = Button(
            self.filter_group,
            width=42,
            height=42,
            relief='flat',
            borderwidth=1,
            image=self.images['grayscale'],
            text='Grayscale',
            compound='top',
            command=lambda: self.start_processing_thread(
                lambda: self.use_filter('grayscale')
            )
        )
        self.grayscale_filter_button.pack(side='left')

        self.sepia_filter_button = Button(
            self.filter_group,
            width=42,
            height=42,
            relief='flat',
            borderwidth=1,
            image=self.images['sepia'],
            text='Sepia',
            compound='top',
            command=lambda: self.start_processing_thread(
                lambda: self.use_filter('sepia')
            )
        )
        self.sepia_filter_button.pack(side='left')

        self.negative_filter_button = Button(
            self.filter_group,
            width=42,
            height=42,
            relief='flat',
            borderwidth=1,
            image=self.images['negative'],
            text='Negative',
            compound='top',
            command=lambda: self.start_processing_thread(
                lambda: self.use_filter('negative')
            )
        )
        self.negative_filter_button.pack(side='left')

        Frame(
            self.control_panel,
            height=1,
            bg='#aaa'
        ).pack(
            fill='x',
            side='top'
        )

        self.tool_group = Frame(
            self.control_panel,
            padx=10,
            pady=10
        )
        self.tool_group.pack(side='top')

        Label(
            self.tool_group,
            text='Tools'
        ).pack(
            side='top',
            fill='x'
        )

        container = Frame(self.tool_group)
        container.pack(side='top', fill='x')

        self.brightness_tool_button = Button(
            container,
            width=50,
            height=42,
            relief='flat',
            borderwidth=1,
            image=self.images['brightness'],
            text='Brightness',
            compound='top',
            command=lambda: self.start_processing_thread(
                lambda: self.use_tool('brightness')
            )
        )
        self.brightness_tool_button.pack(side='left')

        self.brightness_scale = Scale(
            container,
            orient='horizontal',
            from_=-255,
            to=255
        )
        self.brightness_scale.pack()

        container = Frame(self.tool_group)
        container.pack(side='top', fill='x')

        self.blur_tool_button = Button(
            container,
            width=50,
            height=42,
            relief='flat',
            borderwidth=1,
            image=self.images['blur'],
            text='Blur',
            compound='top',
            command=lambda: self.start_processing_thread(
                lambda: self.use_tool('blur')
            )
        )
        self.blur_tool_button.pack(side='left')

        self.blur_scale = Scale(
            container,
            orient='horizontal',
            from_=0.5,
            to=5,
            resolution=0.1
        )
        self.blur_scale.pack()

        Frame(
            self.control_panel,
            height=1,
            bg='#aaa'
        ).pack(
            fill='x',
            side='top'
        )

        self.clean_all_button = Button(
            self.control_panel,
            width=42,
            height=42,
            relief='flat',
            borderwidth=1,
            image=self.images['clean'],
            text='Clean all',
            compound='top',
            command=lambda: self.start_processing_thread(
                lambda: self.use_filter('clean')
            )
        )
        self.clean_all_button.pack()

        Frame(
            self.control_panel,
            height=1,
            bg='#aaa'
        ).pack(
            fill='x',
            side='top'
        )

        Frame(
            self.root,
            width=1,
            bg='#aaa'
        ).pack(
            fill='y',
            side='left'
        )

        # Creating of Display.
        self.display_container = Frame(self.root)
        self.display = DisplayCanvas(
            self.display_container,
            highlightthickness=0
        )
        self.display.place(x=0, y=0)

        self.display.bind('<Button-1>', self.create_operation)
        self.display.bind('<B1-Motion>', self.add_operation)
        self.display.bind('<Shift-Button-1>', lambda e: self.create_operation(e, True))
        self.display.bind('<Shift-B1-Motion>', lambda e: self.add_operation(e, True))
        self.display.bind('<ButtonRelease-1>', self.end_operation)
        self.display.bind('<Control-Button-3>', self.change_brush_color)
        self.display.bind(
            "<Control-MouseWheel>", lambda e: self.move_display_x(e, True)
        )
        self.display.bind(
            "<MouseWheel>", lambda e: self.move_display_y(e, True)
        )

        self.scroll_x = Scale(
            self.root,
            orient='horizontal',
            sliderlength=0,
            from_=0,
            to=0,
            showvalue=0,
            command=self.move_display_x
        )

        container = Frame(self.root)
        container.pack(side='right', fill='y')
        split_block = Frame(container, height=24)
        split_block.pack(side='bottom')

        self.scroll_y = Scale(
            container,
            orient='vertical',
            sliderlength=0,
            from_=0,
            to=0,
            showvalue=0,
            command=self.move_display_y)
        self.scroll_y.pack(side='right', fill='y')

        self.display_container.pack(expand=1, fill='both')
        self.scroll_x.pack(fill='x')

        self.controls = {
            self.open_file_button,
            self.create_file_button,
            self.save_file_button,
            self.save_file_as_button,
            self.close_button,
            self.undo_button,
            self.redo_button,
            self.help_button,
            self.brush_color_changer,
            self.background_color_changer,
            self.color_changers_swap,
            self.brush_size_changer,
            self.brush_form_changer,
            self.brush_button,
            self.eraser_button,
            self.spray_button,
            self.zoom_changer,
            self.grayscale_filter_button,
            self.negative_filter_button,
            self.sepia_filter_button,
            self.brightness_tool_button,
            self.brightness_scale,
            self.blur_tool_button,
            self.blur_scale,
            self.clean_all_button,
            self.display
        }

        self.reset()
        self.save_status = None
        self.zoom_ratio = 1
        self.line_start = None
        self.help_running = False
        self.color_chose_running = False
        self.updating = False
        self.updating_thread = None
        self.processing = False
        self.processing_thread = None

        if path_ is not None:
            self.open_file(path_=path_)

        self.root.mainloop()

    def progress_step(self, start=False, stop=False):
        if start:
            self.progress_indicator.config(bg='#39f')
        elif stop:
            self.progress_indicator.config(width=200)
            sleep(0.1)
            self.progress_indicator.config(width=0, bg='#ccc')
        else:
            width = self.progress_indicator.winfo_width()
            self.progress_indicator.config(width=width+2)

    def start_updating_thread(self):
        """Запуск потока, осуществляющего отрисовку."""
        self.updating_thread = Thread(target=self.do_operation)
        self.updating = True
        self.updating_thread.start()

    def start_processing_thread(self, method):
        """Запуск потока для выполнения обработчика изображения."""
        self.processing_thread = Thread(target=method)
        self.processing_thread.start()

    def move_display_x(self, event, m=None):
        if self.workspace is None:
            return
        if m:
            self.scroll_x.set(self.scroll_x.get() - event.delta/4)
        else:
            self.display.place(x='-' + event, y=self.display.winfo_y())

    def move_display_y(self, event, m=None):
        if self.workspace is None:
            return
        if m:
            self.scroll_y.set(self.scroll_y.get() - event.delta/4)
        else:
            self.display.place(x=self.display.winfo_x(), y='-' + event)

    def reset(self):
        """Сброс установок интерфейса."""
        self.workspace = None
        self.display.delete("all")
        self.display.config(width=0, height=0, cursor='')
        self.brush_button.config(relief='flat')
        self.eraser_button.config(relief='flat')
        self.spray_button.config(relief='flat')
        self.brush_color_changer.config(
            bg='#f0f0f0'
        )
        self.background_color_changer.config(
            bg='#f0f0f0'
        )
        menu = self.brush_form_changer["menu"]
        menu.delete(0, "end")
        self.variable.set('')
        self.brush_size_changer.set(1)
        self.zoom_changer.set(1)
        self.zoom_ratio = 1
        self.blur_scale.set(0.5)
        self.brightness_scale.set(0)
        self.scroll_y.config(to=0, sliderlength=0)
        self.scroll_x.config(to=0, sliderlength=0)
        self.disable(
            self.controls.difference(
                {
                    self.create_file_button,
                    self.open_file_button,
                    self.help_button
                 }
            )
        )
        self.status_indicator.config(text='Ready')

    def initialize(self):
        self.brush_button.config(relief='sunken')
        self.brush_color_changer.config(
            bg=self.workspace.brush.color.to_hex()
        )
        self.background_color_changer.config(
            bg=self.workspace.canvas.background_color.to_hex()
        )
        self.brush_size_changer.config(
            to=len(self.workspace.brush.masks[self.workspace.brush.form])
        )
        self.display.config(
            width=self.workspace.canvas.width,
            height=self.workspace.canvas.height,
            cursor='target'
        )
        menu = self.brush_form_changer["menu"]
        brushes_list = list(self.workspace.brush.masks.keys())
        brushes_list.sort()
        for brush in brushes_list:
            menu.add_command(
                label=brush.capitalize(),
                command=lambda value=brush: self.menu_command(value))
        self.variable.set(brushes_list[0].capitalize())

    def size_recalculate(self, event):
        if self.workspace is None:
            return
        display_w = self.display.winfo_width()
        container_w = self.display_container.winfo_width()
        scroll_w = display_w - container_w
        display_h = self.display.winfo_height()
        container_h = self.display_container.winfo_height()
        scroll_h = display_h - container_h
        if scroll_w > 0:
            slider_w = (container_w ** 2) // display_w
            self.scroll_x.config(to=scroll_w, sliderlength=slider_w)
        else:
            self.scroll_x.config(to=0, sliderlength=0)
        if scroll_h > 0:
            slider_h = (container_h ** 2) // display_h
            self.scroll_y.config(to=scroll_h, sliderlength=slider_h)
        else:
            self.scroll_y.config(to=0, sliderlength=0)

    def enable(self, targets):
        """Разблокировка элементов интерфейса."""
        for target in targets:
            target.config(state='normal')

    def disable(self, targets):
        """Блокировка элементов интерфейса."""
        for target in targets:
            target.config(state='disabled')

    def destroy(self):
        """Обработчик закрытия окна."""
        if self.close():
            self.root.destroy()

    def help(self):
        """Справка."""
        if self.help_running:
            return
        dialog = Toplevel()
        self.help_running = True
        dialog.resizable(False, False)
        dialog.focus()
        try:
            with open('help.txt', encoding='utf-8') as file_:
                text = file_.read()
        except FileNotFoundError:
            messagebox.showerror(
                title='File open error!',
                message='Help file not found'
            )
            return
        except PermissionError:
            messagebox.showerror(
                title='File open error!',
                message='No access to the help file'
            )
            return
        if os_name == 'nt':
            dialog.iconbitmap(bitmap='images/Help.ico')
        dialog.text = Label(
            dialog, text=text, justify='left'
        )
        dialog.title('Help!')
        dialog.bind('<Destroy>', self.help_free)
        dialog.text.pack()

    def help_free(self, event):
        self.help_running = False

    def close(self):
        """Закрытие и сохранение открытого файла."""
        if self.workspace is None:
            return True
        if self.processing:
            return False
        self.updating = False
        self.updating_thread.join()
        if self.save_status != 0:
            answer = messagebox.askyesnocancel(
                title='Closing and saving',
                message='Do you want to save your progress?'
            )
        else:
            answer = False
        if answer is None:
            self.start_updating_thread()
            return False
        if answer:
            if self.save_file():
                self.reset()
                return True
            else:
                self.start_updating_thread()
                return False
        else:
            self.reset()
            return True

    def create_file_dialog(self, event=None):
        """Диалог создания нового файла."""
        if self.close():
            self.status_indicator.config(text='Creating...')
            dialog = Toplevel()
            if os_name == 'nt':
                dialog.iconbitmap(bitmap='images/Plus.ico')
            Label(dialog, text='Width in pixels:').pack()
            width_field = Entry(dialog)
            width_field.insert(0, '300')
            width_field.pack()
            Label(dialog, text='Height in pixels:').pack()
            height_field = Entry(dialog)
            height_field.insert(0, '300')
            height_field.pack()
            Label(dialog, text='Resolution:').pack()
            resolution_field = Entry(dialog)
            resolution_field.insert(0, '96')
            resolution_field.pack()
            Label(dialog, text='Background color:').pack()
            color_field = Button(
                dialog, width=4, height=2, relief='raise', borderwidth=1,
                background='#FFFFFF', command=lambda: color_field.config(
                    bg=self._block_create_dialog(color_field)
                )
            )
            color_field.pack()
            dialog.message = Label(dialog, anchor='center')
            dialog.message.pack(side='bottom', fill='x')
            ok_button = Button(
                dialog, text='OK',
                command=lambda: self.create_file(
                    dialog,
                    width_field,
                    height_field,
                    resolution_field,
                    color_field
                )
            )
            ok_button.pack(side='bottom')
            cancel_button = Button(
                dialog, text='Cancel',
                command=lambda: self._close_create_dialog(dialog)
            )
            cancel_button.pack(side='bottom')
            dialog.bind(
                '<Return>',
                lambda _: self.create_file(
                    dialog,
                    width_field,
                    height_field,
                    resolution_field,
                    color_field
                )
            )
            dialog.bind('<Escape>', lambda _: dialog.destroy())
            dialog.title('Create new file')
            dialog_width = 300
            dialog_height = 300
            shift_x = (self.root.winfo_screenwidth() - dialog_width) // 2
            if shift_x < 0:
                shift_x = 0
            shift_y = (self.root.winfo_screenheight() - dialog_height) // 2
            if shift_y < 0:
                shift_y = 0
            dialog.geometry('{}x{}+{}+{}'.format(dialog_width, dialog_height,
                                                 shift_x, shift_y))
            dialog.transient(self.root)
            dialog.resizable(False, False)
            dialog.grab_set()
            dialog.focus()
            self.root.wait_window(dialog)
            if self.workspace is not None:
                self.status_indicator.config(text='Ready')

    def _block_create_dialog(self, color_field):
        if self.color_chose_running:
            return color_field.cget('bg')
        self.color_chose_running = True
        result = colorchooser.askcolor(color_field.cget('bg'))[1]
        self.color_chose_running = False
        return result

    def _close_create_dialog(self, dialog):
        if not self.color_chose_running:
            dialog.destroy()

    def create_file(self, dialog, width_f, height_f, resolution_f, color_f):
        """Создание нового файла."""
        if self.color_chose_running:
            return
        width = width_f.get()
        height = height_f.get()
        resolution = resolution_f.get()
        background = Color(hex_=color_f.cget('bg'))
        try:
            width = int(width)
            height = int(height)
            resolution = int(resolution)
        except ValueError:
            messagebox.showwarning(
                title='Input error!',
                message='Values must be integer numbers'
            )
            return
        if width <= 0 and height <= 0 and resolution <= 0:
            messagebox.showwarning(
                title='Input error!',
                message='Values must be positive'
            )
            return
        if height * width > 4000000:
            messagebox.showwarning(
                title='Input error!',
                message='Size is too big'
            )
            return
        dialog.destroy()
        try:
            self.workspace = WorkSpace(
                width=width,
                height=height,
                resolution=resolution,
                background=background
            )
        except FileOpenError as error_:
            messagebox.showerror(
                title='File open error!',
                message=error_.value
            )
            if error_.critical:
                self.workspace = None
                self.destroy()
                return
        self.initialize()
        self.render()
        self.enable(
            self.controls.difference(
                {
                    self.undo_button,
                    self.redo_button
                }
            )
        )
        self.save_status = None
        self.start_updating_thread()
        self.status_indicator.config(text='Ready')

    def open_file(self, event=None, path_=None):
        """Открытие файла."""
        if self.close():
            self.status_indicator.config(text='Opening...')
            types = [('Bitmap image', '*.bmp')]
            if path_ is None:
                path_ = filedialog.askopenfilename(filetypes=types)
            if path_ == '':
                self.status_indicator.config(text='Ready')
                return
            while True:
                try:
                    with open(path_, mode='rb') as file_:
                        self.workspace = WorkSpace(file_=file_)
                        break
                except FileNotFoundError:
                    retry = messagebox.askretrycancel(
                        title='File open error!',
                        message='File not found'
                    )
                except PermissionError:
                    retry = messagebox.askretrycancel(
                        title='File open error!',
                        message='No access to file'
                    )
                except FileOpenError as error_:
                    retry = messagebox.askretrycancel(
                        title='File open error!', message=error_.value
                    )
                    if error_.critical:
                        self.destroy()
                if not retry:
                    self.status_indicator.config(text='Ready')
                    return
            self.initialize()
            self.render()
            self.enable(
                self.controls.difference(
                    {
                        self.undo_button,
                        self.redo_button,
                        self.save_file_button
                    }
                )
            )
            self.save_status = 0
            self.start_updating_thread()
            self.status_indicator.config(text='Ready')

    def save_file(self, event=None):
        """Сохранение открытого файла."""
        if self.workspace is None:
            return True
        if self.workspace.file_path is None:
            return self.save_file_as()
        self.status_indicator.config(text='Saving...')
        while True:
            try:
                with open(self.workspace.file_path, mode='wb') as file_:
                    self.workspace.image_info.pack(
                        file_, self.workspace.canvas
                    )
                    break
            except FileNotFoundError:
                retry = messagebox.askretrycancel(
                    title='File save error!',
                    message='File not found'
                )
            except PermissionError:
                retry = messagebox.askretrycancel(
                    title='File save error!',
                    message='No access to file'
                )
            if not retry:
                self.status_indicator.config(text='Ready')
                return False
        self.status_indicator.config(text='Ready')
        self.save_file_button.config(state='disabled')
        self.save_status = 0
        return True

    def save_file_as(self):
        """Сохранение открытого файла в новый файл."""
        if self.workspace is None:
            return True
        self.status_indicator.config(text='Saving...')
        types = [('Bitmap Image', '*.bmp')]
        path_ = filedialog.asksaveasfilename(filetypes=types)
        if path_ == '':
            self.status_indicator.config(text='Ready')
            return False
        while True:
            try:
                with open(path_, mode='wb') as file_:
                    self.workspace.image_info.pack(file_, self.workspace.canvas)
                    break
            except FileNotFoundError:
                retry = messagebox.askretrycancel(
                    title='File save error!',
                    message='File not found'
                )
            except PermissionError:
                retry = messagebox.askretrycancel(
                    title='File save error!',
                    message='No access to file'
                )
            if not retry:
                self.status_indicator.config(text='Ready')
                return False
        self.workspace.file_path = path_
        self.status_indicator.config(text='Ready')
        self.save_file_button.config(state='disabled')
        self.save_status = 0
        return True

    def render(self, start=None, end=None):
        """Отрисовка."""
        if self.workspace is None:
            return
        if start is None:
            start = Point(0, 0)
        if end is None:
            end = Point(self.workspace.canvas.grid_width - 1,
                        self.workspace.canvas.grid_height - 1)
        for j in range(start.x, end.x + 1):
            for i in range(start.y, end.y + 1):
                image_orig = self.workspace.canvas.image[i][j]
                image = image_orig.resize(
                    (
                        image_orig.width * self.zoom_ratio,
                        image_orig.height * self.zoom_ratio
                     )
                )
                coordinates = self.workspace.canvas.block_coordinates(i, j)
                cell = ImageTk.PhotoImage(image)
                self.display.create_image(
                    coordinates.x * self.zoom_ratio,
                    coordinates.y * self.zoom_ratio,
                    image=cell,
                    anchor='nw'
                )
                self.workspace.grid[i][j] = cell

    def create_operation(self, event, line=False):
        if self.workspace is not None:
            if not self.processing:
                self.add_operation(event, line)

    def add_operation(self, event, line=False):
        if line:
            if self.line_start is not None:
                begin = self.line_start
                end = event
                if begin.x > end.x:
                    begin, end = Point(end.x, end.y), Point(begin.x, begin.y)
                dx = begin.x - end.x
                dy = begin.y - end.y
                if dx != 0:
                    k = dy / dx
                    if k
            else:
                self.line_start = event
        if self.workspace is not None:
            if not self.processing:
                self.workspace.operation_queue.append(
                    Point(
                        event.x // self.zoom_ratio + 1,
                        event.y // self.zoom_ratio + 1
                    )
                )

    def end_operation(self, event):
        if self.workspace is not None:
            if not self.processing:
                self.add_operation(event)
                self.workspace.operation_queue.append(None)
                self.line_start = None

    def do_operation(self):
        """Выполнение операции."""
        integral_operation = []
        begin = None
        disabled = False
        min_from = Point(
            self.workspace.canvas.grid_width - 1,
            self.workspace.canvas.grid_height - 1
        )
        max_to = Point(0, 0)
        while self.updating:
            if self.workspace is None or len(self.workspace.operation_queue) == 0:
                continue
            if not disabled:
                self.disable(self.controls)
                disabled = True
            end = self.workspace.operation_queue.pop(0)
            if end is None:
                integral_operation.insert(0, (min_from, max_to))
                min_from = Point(self.workspace.canvas.grid_width - 1,
                                 self.workspace.canvas.grid_height - 1)
                max_to = Point(0, 0)
                self.workspace.undo_stack.append(integral_operation)
                integral_operation = []
                self.workspace.redo_stack.clear()
                begin = None
                self.enable(
                    self.controls.difference({self.redo_button})
                )
                disabled = False
                if self.save_status is not None:
                    if self.save_status < 0:
                        self.save_status = None
                    else:
                        self.save_status += 1
                continue
            if begin is None:
                changed, min_, max_ = self.workspace.brush.draw_point(
                    end
                )
            else:
                changed, min_, max_ = self.workspace.brush.draw_line(
                    begin, end
                )
            integral_operation.extend(changed)
            begin = end
            from_ = Point(min_.x // self.workspace.canvas.block_size,
                          min_.y // self.workspace.canvas.block_size)
            to = Point(max_.x // self.workspace.canvas.block_size,
                       max_.y // self.workspace.canvas.block_size)
            if from_.x < 0:
                from_.x = 0
            if from_.y < 0:
                from_.y = 0
            if to.x < 0:
                to.x = 0
            if to.y < 0:
                to.y = 0
            if from_.x >= self.workspace.canvas.grid_width:
                from_.x = self.workspace.canvas.grid_width - 1
            if from_.y >= self.workspace.canvas.grid_height:
                from_.y = self.workspace.canvas.grid_height - 1
            if to.x >= self.workspace.canvas.grid_width:
                to.x = self.workspace.canvas.grid_width - 1
            if to.y >= self.workspace.canvas.grid_height:
                to.y = self.workspace.canvas.grid_height - 1
            self.render(from_, to)
            min_from = Point(
                min(min_from.x, from_.x), min(min_from.y, from_.y)
            )
            max_to = Point(
                max(max_to.x, to.x), max(max_to.y, to.y)
            )

    def undo_operation(self):
        """Отмена последней операции."""
        if self.workspace is None:
            return
        changed = []
        operation = self.workspace.undo_stack.pop(-1)
        for i in range(len(operation) - 1, 0, -1):
            changed.append((operation[i][0], self.workspace.canvas[
                operation[i][0]
            ]))
            self.workspace.canvas[operation[i][0]] = operation[i][1]
        changed.insert(0, operation[0])
        self.workspace.redo_stack.append(changed)
        self.render(*operation[0])
        self.handle_change(-1)

    def redo_operation(self):
        """Повтор последней операции."""
        if self.workspace is None:
            return
        changed = []
        operation = self.workspace.redo_stack.pop(-1)
        for i in range(len(operation) - 1, 0, -1):
            changed.append((operation[i][0], self.workspace.canvas[
                operation[i][0]
            ]))
            self.workspace.canvas[operation[i][0]] = operation[i][1]
        changed.insert(0, operation[0])
        self.workspace.undo_stack.append(changed)
        self.render(*operation[0])
        self.handle_change(1)

    def use_filter(self, type_):
        """Применение фильтра."""
        if self.workspace is None:
            return
        self.processing = True
        self.disable(self.controls.difference(
            {
                self.brush_color_changer,
                self.background_color_changer,
                self.color_changers_swap,
                self.brush_size_changer,
                self.brush_form_changer,
                self.brush_button,
                self.eraser_button,
                self.spray_button,
                self.help_button,
                self.blur_scale,
                self.brightness_scale
            }
        ))
        changed = []
        if type_ == 'grayscale':
            changed = Filter.grayscale(
                self.workspace.canvas,
                self.progress_step
            )
        if type_ == 'negative':
            changed = Filter.negative(
                self.workspace.canvas,
                self.progress_step)
        if type_ == 'sepia':
            changed = Filter.sepia(
                self.workspace.canvas,
                self.progress_step
            )
        if type_ == 'clean':
            changed = Filter.clean(
                self.workspace.canvas,
                self.progress_step
            )
        changed.insert(0, (None, None))
        self.workspace.undo_stack.append(changed)
        self.workspace.redo_stack.clear()
        self.render()
        self.handle_change(1)
        self.processing = False

    def use_tool(self, type_):
        """Применение инструмента."""
        if self.workspace is None:
            return
        self.processing = True
        self.disable(self.controls.difference(
            {
                self.brush_color_changer,
                self.background_color_changer,
                self.color_changers_swap,
                self.brush_size_changer,
                self.brush_form_changer,
                self.brush_button,
                self.eraser_button,
                self.spray_button,
                self.help_button,
                self.blur_scale,
                self.brightness_scale
            }
        ))
        changed = []
        if type_ == 'blur':
            size_ = self.workspace.canvas.height * self.workspace.canvas.width
            if size_ > 100000:
                result = messagebox.askokcancel(
                    title='Warning!',
                    message='This tool works very slowly on big images. '
                            'Do you really want to continue?'
                )
            else:
                result = True
            if result:
                changed = Tools.gaussian_blur(
                    self.workspace.canvas,
                    self.blur_scale.get(),
                    self.progress_step
                )
        if type_ == 'brightness':
            if self.brightness_scale.get() != 0:
                changed = Tools.brightness(
                    self.workspace.canvas,
                    self.brightness_scale.get(),
                    self.progress_step
                )
        changed.insert(0, (None, None))
        self.workspace.undo_stack.append(changed)
        self.workspace.redo_stack.clear()
        self.render()
        self.handle_change(1)
        self.processing = False

    def handle_change(self, dir_):
        self.enable(self.controls.difference(
            {self.undo_button, self.redo_button}
        ))
        if dir_ == 1:
            if self.save_status is not None:
                self.save_status += 1
            if len(self.workspace.redo_stack) == 0:
                self.redo_button.config(state='disabled')
            else:
                self.redo_button.config(state='normal')
            self.undo_button.config(state='normal')
        elif dir_ == -1:
            if self.save_status is not None:
                self.save_status -= 1
            if len(self.workspace.undo_stack) == 0:
                self.undo_button.config(state='disabled')
            else:
                self.undo_button.config(state='normal')
            self.redo_button.config(state='normal')
        if self.save_status == 0:
            self.save_file_button.config(state='disabled')

    def zoom(self, value):
        if self.workspace is None:
            return
        self.zoom_ratio = int(value)
        self.display.config(
            width=self.workspace.canvas.width * self.zoom_ratio,
            height=self.workspace.canvas.height * self.zoom_ratio,
        )
        self.render()

    def change_background_color(self, color=None):
        if self.workspace is None:
            return
        if color is None:
            color = colorchooser.askcolor(
                self.workspace.canvas.background_color.to_hex()
            )[1]
        if color is not None:
            self.workspace.canvas.background_color = Color(hex_=color)
            self.background_color_changer.config(bg=color)

    def change_brush_color(self, event=None, color=None):
        if self.workspace is None:
            return
        if color is None:
            if event is not None:
                self.workspace.brush.pick(Point(event.x, event.y))
                self.background_color_changer.config(
                    bg=self.workspace.canvas.background_color.to_hex()
                )
                self.brush_color_changer.config(
                    bg=self.workspace.brush.color.to_hex()
                )
            else:
                color = colorchooser.askcolor(
                    self.workspace.brush.color.to_hex()
                )[1]  # todo
                if color is not None:
                    self.workspace.brush.color = Color(hex_=color)
                    self.brush_color_changer.config(bg=color)
        else:
            self.workspace.brush.color = Color(hex_=color)
            self.brush_color_changer.config(bg=color)

    def swap_color_changers(self):
        brush_color = self.brush_color_changer.cget('bg')
        background_color = self.background_color_changer.cget('bg')
        self.change_brush_color(color=background_color)
        self.change_background_color(color=brush_color)

    def change_brush_type(self, type_):
        if self.workspace is None:
            return
        if type_ == 'brush':
            self.brush_button.config(relief='sunken')
            self.eraser_button.config(relief='flat')
            self.spray_button.config(relief='flat')
        elif type_ == 'eraser':
            self.brush_button.config(relief='flat')
            self.eraser_button.config(relief='sunken')
            self.spray_button.config(relief='flat')
        elif type_ == 'spray':
            self.brush_button.config(relief='flat')
            self.eraser_button.config(relief='flat')
            self.spray_button.config(relief='sunken')
        else:
            raise ValueError()
        self.workspace.brush.type_ = type_

    def change_brush_form(self, form):
        if self.workspace is not None:
            self.workspace.brush.form = form

    def change_brush_size(self, event):
        if self.workspace is not None:
            self.workspace.brush.size = int(event)

    def menu_command(self, value):
        self.variable.set(value.capitalize())
        self.change_brush_form(value)
        self.brush_size_changer.config(
            to=len(self.workspace.brush.masks[self.workspace.brush.form])
        )


class CLI:
    """Консольный интерфейс приложения."""

    def __init__(self, path_, command, value=None, logging_=False):
        if logging_:
            basicConfig(filename='gredit.log')
        basicConfig(level=INFO)
        info('Gredit is running')
        self.path_ = path_
        self.command = command
        if value is not None and value.is_didgit():
            self.value = int(value)
        self.workspace = None
        if self.open_file():
            self.evaluate_command()
            self.save_file()

    def open_file(self):
        try:
            with open(self.path_, mode='rb') as file_:
                self.workspace = WorkSpace(file_=file_)
        except FileNotFoundError:
            error(u'File not found')
            return False
        except PermissionError:
            error(u'No access to file')
            return False
        except FileOpenError as error_:
            error(error_.value)
            return False
        return True

    def evaluate_command(self):
        if self.command == 'grayscale':
            Filter.grayscale(self.workspace.canvas, None)
        elif self.command == 'negative':
            Filter.negative(self.workspace.canvas, None)
        elif self.command == 'sepia':
            Filter.sepia(self.workspace.canvas, None)
        elif self.command == 'blur':
            if 0.5 <= self.value <= 5:
                Tools.gaussian_blur(
                    self.workspace.canvas,
                    self.value,
                    None
                )
            else:
                error(u'Wrong value')
        elif self.command == 'brightness':
            if self.value != 0:
                Tools.brightness(
                    self.workspace.canvas,
                    self.value,
                    None
                )

    def save_file(self):
        try:
            with open(self.path_, mode='wb') as file_:
                self.workspace.image_info.pack(file_, self.workspace.canvas)
        except FileNotFoundError:
            error(u'File not found')
            return
        except PermissionError:
            error(u'File not found')
            return


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument(
        '-f', '--filter',
        nargs=2,
        metavar=('PATH', 'FILTER'),
        help='Open file by path and applies the specified filter to image'
    )
    parser.add_argument(
        '-t', '--tool',
        nargs=3,
        metavar=('PATH', 'TOOL', 'VALUE'),
        help='Open file by path and use the specified tool on image'
    )
    parser.add_argument(
        '-o', '--open',
        dest='path',
        help='Open file by path to edit'
    )
    parser.add_argument(
        '-l', '--logging',
        dest='log',
        help='Types logs to the gredit.log'
    )
    args = parser.parse_args()
    logging_ = args.log is not None
    if args.path is not None:
        GUI(width=1280, height=720, path_=args.path)
    elif args.filter is not None:
        CLI(
            path_=args.filter[0],
            command=args.filter[1],
            logging_=logging_
        )
    elif args.tool is not None:
        CLI(
            path_=args.tool[0],
            command=args.tool[1],
            value=args.tool[2],
            logging_=logging_
        )
    else:
        GUI(width=1280, height=720)
