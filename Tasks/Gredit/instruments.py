from math import ceil, e
from copy import deepcopy
from random import randint
from os import listdir

from utils import Colors

from basics import Point, Color, FileOpenError


class Brush:
    """Используется для редактирования изображания в WorkingCanvas."""

    def __init__(self, canvas, color, size, type_, form):
        self.canvas = canvas
        self.color = color
        self.size = size
        self.type_ = type_
        self.masks = {}
        try:
            brushes = listdir('brushes')
        except FileNotFoundError:
            raise FileOpenError('Brush directory is not found', True)
        except PermissionError:
            raise FileOpenError('No access to brush directory', True)
        exception_count = 0
        if len(brushes) == 0:
            raise FileOpenError('No available brush files', True)
        for brush in brushes:
            if brush.endswith('.brush'):
                try:
                    with open('brushes/{}'.format(brush)) as brush_file:
                        name = brush[:-6].lower()
                        mask = Brush._parse_brush_file(brush_file)
                        self.masks[name] = mask
                except (FileNotFoundError, PermissionError, FileOpenError):
                    exception_count += 1
        if exception_count == len(brushes):
            raise FileOpenError('No intact brush files', True)
        self.form = form if form in self.masks else list(self.masks.keys())[0]
        self.handlers = {
            'brush': (self.brush_drawer, self.brush_picker),
            'eraser': (self.eraser_drawer, self.eraser_picker),
            'spray': (self.spray_drawer, self.spray_picker)
        }

    @staticmethod
    def _parse_brush_file(file_):
        """Проверяет полученную из файла маску кисти на корректность."""
        masks = []
        content = file_.read()
        lines = content.split('\n')
        data = ' '.join(lines).replace('  ', ' ').split()
        if len(data) == 0:
            raise FileOpenError(
                'Brush file is corrupted', False
            )
        n = 1
        index = 0
        while index < len(data):
            masks.append([])
            for i in range(n):
                if index == len(data):
                    raise FileOpenError(
                        'Brush file is corrupted', False
                    )
                try:
                    masks[-1].append(list(map(bool, map(int, data[index]))))
                except ValueError:
                    raise FileOpenError(
                        'Brush file is corrupted', False
                    )
                if len(masks[-1][-1]) > n:
                    raise FileOpenError(
                        'Brush file is corrupted', False
                    )
                index += 1
            n += 1
        return masks

    @property
    def mask(self):
        return self.masks[self.form][self.size - 1]

    def draw_pixel(self, x, y, changed):
        """Изменение пикселя изображения."""
        pixel = Point(x, y)
        x_limit = self.canvas.width - 1
        y_limit = self.canvas.height - 1
        if pixel.is_within(0, x_limit, 0, y_limit):
            old_color = self.canvas[pixel]
            is_changed = self.handlers[self.type_][0](pixel)
            if is_changed:
                changed.append((pixel, old_color))

    def draw_point(self, position):
        changed = []
        offset = -self.size // 2
        for i in range(self.size):
            for j in range(self.size):
                if self.mask[j][i]:
                    pixel = position.shift(i + offset, j + offset)
                    self.draw_pixel(pixel.x, pixel.y, changed)
        min_changed = position.shift(offset, offset)
        max_changed = position.shift(-offset, -offset)
        return changed, min_changed, max_changed

    def draw_line(self, begin, end):
        changed = []
        offset = -self.size // 2
        if begin.x > end.x:
            begin, end = Point(end.x, end.y), Point(begin.x, begin.y)
        if begin.x == end.x:
            if begin.y > end.y:
                begin, end = Point(begin.x, end.y), Point(end.x, begin.y)
            for i in range(self.size):
                for j in range(self.size):
                    if self.mask[j][i]:
                        for y in range((end.y - begin.y) + 1):
                            self.draw_pixel(
                                begin.x + i + offset,
                                begin.y + y + j + offset,
                                changed
                            )
        else:
            k = (end.y - begin.y) / (end.x - begin.x)
            if -1 < k < 1:
                for i in range(self.size):
                    for j in range(self.size):
                        if self.mask[j][i]:
                            for x in range((end.x - begin.x) + 1):
                                self.draw_pixel(
                                    begin.x + x + i + offset,
                                    begin.y + round(k * x) + j + offset,
                                    changed
                                )
            else:
                if end.y > begin.y:
                    step = 1
                else:
                    step = -1
                for i in range(self.size):
                    for j in range(self.size):
                        if self.mask[j][i]:
                            for y in range(0, (end.y - begin.y), step):
                                self.draw_pixel(
                                    begin.x + round(y / k) + i + offset,
                                    begin.y + y + j + offset,
                                    changed
                                )
        min_changed = begin.shift(offset, offset)
        max_changed = end.shift(-offset, -offset)
        if min_changed.y > max_changed.y:
            min_changed, max_changed = Point(min_changed.x, max_changed.y), \
                                       Point(max_changed.x, min_changed.y)
        return changed, min_changed, max_changed

    def pick(self, position):
        return self.handlers[self.type_][1](position)

# region Handlers todo made static
    def brush_drawer(self, pixel):
        if self.canvas[pixel] != self.color:
            self.canvas[pixel] = self.color
            return True
        return False

    def brush_picker(self, pixel):
        self.color = self.canvas[pixel]

    def eraser_drawer(self, pixel):
        if self.canvas[pixel] != self.canvas.background_color:
            self.canvas[pixel] = self.canvas.background_color
            return True
        return False

    def eraser_picker(self, pixel):
        self.canvas.background_color = self.canvas[pixel]

    def spray_drawer(self, pixel):
        if self.canvas[pixel] != self.color:
            if randint(0, 9) < 1:
                self.canvas[pixel] = self.color
            return True
        return False

    def spray_picker(self, pixel):
        self.color = self.canvas[pixel]
# endregion


class Filter:

    @staticmethod
    def grayscale(canvas, step):
        if step is not None:
            step(start=True)
        changed = []
        brush = Brush(
            canvas=canvas,
            size=1,
            color=Colors.black,
            type_='brush',
            form='circle'
        )
        increment = (canvas.width * canvas.height) // 100
        count = 0
        for y in range(canvas.height):
            for x in range(canvas.width):
                if step is not None:
                    if count % increment == 0:
                        step()
                    count += 1
                pixel = canvas[(x, y)]
                color = int(0.21*pixel.red + 0.72*pixel.green + 0.07*pixel.blue)
                brush.color = Color(color, color, color)
                brush.draw_pixel(x, y, changed)
        if step is not None:
            step(stop=True)
        return changed

    @staticmethod
    def negative(canvas, step):
        if step is not None:
            step(start=True)
        changed = []
        brush = Brush(
            canvas=canvas,
            size=1,
            color=Colors.black,
            type_='brush',
            form='circle'
        )
        increment = (canvas.width * canvas.height) // 100
        count = 0
        for y in range(canvas.height):
            for x in range(canvas.width):
                if step is not None:
                    if count % increment == 0:
                        step()
                    count += 1
                pixel = canvas[(x, y)]
                brush.color = Color(
                    255 - pixel.red, 255 - pixel.green, 255 - pixel.blue
                )
                brush.draw_pixel(x, y, changed)
        if step is not None:
            step(stop=True)
        return changed

    @staticmethod
    def sepia(canvas, step):
        if step is not None:
            step(start=True)
        changed = []
        brush = Brush(
            canvas=canvas,
            size=1,
            color=Colors.black,
            type_='brush',
            form='circle'
        )
        increment = (canvas.width * canvas.height) // 100
        count = 0
        for y in range(canvas.height):
            for x in range(canvas.width):
                if step is not None:
                    if count % increment == 0:
                        step()
                    count += 1
                pixel = canvas[(x, y)]
                brush.color = Color(
                    int(pixel.red*0.393 + pixel.green*0.769 + pixel.blue*0.189),
                    int(pixel.red*0.349 + pixel.green*0.686 + pixel.blue*0.168),
                    int(pixel.red*0.272 + pixel.green*0.534 + pixel.blue*0.131)
                )
                brush.draw_pixel(x, y, changed)
        if step is not None:
            step(stop=True)
        return changed

    @staticmethod
    def clean(canvas, step):
        if step is not None:
            step(start=True)
        changed = []
        brush = Brush(
            canvas=canvas,
            size=1,
            color=canvas.background_color,
            type_='brush',
            form='circle'
        )
        increment = (canvas.width * canvas.height) // 100
        count = 0
        for y in range(canvas.height):
            for x in range(canvas.width):
                if step is not None:
                    if count % increment == 0:
                        step()
                    count += 1
                brush.draw_pixel(x, y, changed)
        if step is not None:
            step(stop=True)
        return changed


class Tools:

    @staticmethod
    def _gaussian_distribution(r):
        window = []
        n = ceil(3 * r)
        window.append(1)
        for i in range(1, n):
            window.append(e ** (-(i * i) / (2 * (r ** 2))))
            window.insert(0, window[-1])
        return window

    @staticmethod
    def gaussian_blur(canvas, radius, step):
        if step is not None:
            step(start=True)
        window = Tools._gaussian_distribution(radius)
        tmp_canvas = deepcopy(canvas)
        changed = []
        brush = Brush(
            canvas=tmp_canvas,
            size=1,
            color=Colors.black,
            type_='brush',
            form='circle'
        )
        increment = (canvas.width * canvas.height) // 50
        count = 0
        for y in range(canvas.height):
            for x in range(canvas.width):
                if step is not None:
                    if count % increment == 0:
                        step()
                    count += 1
                sum_ = 0
                brush.color = Color(0, 0, 0)
                for i in range(-len(window) // 2, len(window) // 2 + 1):
                    if 0 <= x + i < canvas.width:
                        pixel = canvas[(x + i, y)]
                        brush.color.red += pixel.red * window[
                            i + len(window) // 2
                        ]
                        brush.color.green += pixel.green * window[
                            i + len(window) // 2
                        ]
                        brush.color.blue += pixel.blue * window[
                            i + len(window) // 2
                        ]
                        sum_ += window[i + len(window) // 2]
                brush.color.red = int(brush.color.red / sum_)
                brush.color.green = int(brush.color.green / sum_)
                brush.color.blue = int(brush.color.blue / sum_)
                brush.draw_pixel(x, y, [])
        brush.canvas = canvas
        for x in range(canvas.width):
            for y in range(canvas.height):
                sum_ = 0
                if step is not None:
                    if count % increment == 0:
                        step()
                    count += 1
                brush.color = Color(0, 0, 0)
                for j in range(-len(window) // 2, len(window) // 2 + 1):
                    if 0 <= y + j < canvas.height:
                        pixel = tmp_canvas[(x, y + j)]
                        brush.color.red += pixel.red * window[
                            j + len(window) // 2
                        ]
                        brush.color.green += pixel.green * window[
                            j + len(window) // 2
                        ]
                        brush.color.blue += pixel.blue * window[
                            j + len(window) // 2
                        ]
                        sum_ += window[j + len(window) // 2]
                brush.color.red = int(brush.color.red / sum_)
                brush.color.green = int(brush.color.green / sum_)
                brush.color.blue = int(brush.color.blue / sum_)
                brush.draw_pixel(x, y, changed)
        if step is not None:
            step(stop=True)
        return changed

    @staticmethod
    def brightness(canvas, delta, step):
        if step is not None:
            step(start=True)
        changed = []
        brush = Brush(
            canvas=canvas,
            size=1,
            color=Colors.black,
            type_='brush',
            form='circle'
        )
        increment = (canvas.width * canvas.height) // 100
        count = 0
        for y in range(canvas.height):
            for x in range(canvas.width):
                if step is not None:
                    if count % increment == 0:
                        step()
                    count += 1
                pixel = canvas[Point(x, y)]
                brush.color = Color(
                    pixel.red + delta, pixel.green + delta, pixel.blue + delta
                )
                brush.draw_pixel(x, y, changed)
        if step is not None:
            step(stop=True)
        return changed
