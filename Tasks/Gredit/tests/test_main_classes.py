import unittest

from basics import Point, Color
from structures import ImageInfo, WorkingCanvas, Brush, WorkSpace
from instruments import Filter, Tools


class TestImageInfo(unittest.TestCase):

    def test_init(self):
        with open('images/1x1.bmp', 'rb') as file:
            image_info = ImageInfo(file_=file)
        self.assertEqual(image_info.height, 1)
        self.assertEqual(image_info.width, 1)
        self.assertEqual(image_info.image[0][0], (255, 0, 0))

        with open('images/2x2.bmp', 'rb') as file:
            image_info = ImageInfo(file_=file)
        self.assertEqual(image_info.height, 2)
        self.assertEqual(image_info.width, 2)
        self.assertEqual(image_info.image[0][0], (255, 0, 0))
        self.assertEqual(image_info.image[1][0], (0, 0, 255))
        self.assertEqual(image_info.image[0][1], (0, 255, 0))
        self.assertEqual(image_info.image[1][1], (255, 255, 0))

        with open('images/3x3.bmp', 'rb') as file:
            image_info = ImageInfo(file_=file)
        self.assertEqual(image_info.height, 3)
        self.assertEqual(image_info.width, 3)
        self.assertEqual(image_info.image[0][0], (255, 0, 0))
        self.assertEqual(image_info.image[1][0], (0, 0, 255))
        self.assertEqual(image_info.image[2][0], (255, 0, 255))
        self.assertEqual(image_info.image[0][1], (0, 255, 0))
        self.assertEqual(image_info.image[1][1], (255, 255, 0))
        self.assertEqual(image_info.image[2][1], (128, 128, 128))
        self.assertEqual(image_info.image[0][2], (0, 0, 0))
        self.assertEqual(image_info.image[1][2], (0, 255, 255))
        self.assertEqual(image_info.image[2][2], (255, 255, 255))

        with open('images/4x4.bmp', 'rb') as file:
            image_info = ImageInfo(file_=file)
        self.assertEqual(image_info.height, 4)
        self.assertEqual(image_info.width, 4)
        self.assertEqual(image_info.image[0][0], (255, 0, 0))
        self.assertEqual(image_info.image[1][0], (0, 0, 255))
        self.assertEqual(image_info.image[2][0], (255, 0, 255))
        self.assertEqual(image_info.image[3][0], (0, 0, 128))
        self.assertEqual(image_info.image[0][1], (0, 255, 0))
        self.assertEqual(image_info.image[1][1], (255, 255, 0))
        self.assertEqual(image_info.image[2][1], (128, 128, 128))
        self.assertEqual(image_info.image[3][1], (128, 0, 128))
        self.assertEqual(image_info.image[0][2], (0, 0, 0))
        self.assertEqual(image_info.image[1][2], (0, 255, 255))
        self.assertEqual(image_info.image[2][2], (255, 255, 255))
        self.assertEqual(image_info.image[3][2], (0, 128, 128))
        self.assertEqual(image_info.image[0][3], (128, 0, 0))
        self.assertEqual(image_info.image[1][3], (0, 128, 0))
        self.assertEqual(image_info.image[2][3], (128, 128, 0))
        self.assertEqual(image_info.image[3][3], (192, 192, 192))


class TestWorkingCanvas(unittest.TestCase):

    def test_init(self):
        with open('images/4x4.bmp', 'rb') as file:
            image_info = ImageInfo(file_=file)
        canvas = WorkingCanvas(image_info=image_info)
        self.assertEqual(canvas.height, 4)
        self.assertEqual(canvas.width, 4)
        self.assertEqual(canvas.grid_height, 1)
        self.assertEqual(canvas.grid_width, 1)
        self.assertEqual(canvas[(0, 0)], Color(255, 0, 0))
        self.assertEqual(canvas[(1, 0)], Color(0, 0, 255))
        self.assertEqual(canvas[(2, 0)], Color(255, 0, 255))
        self.assertEqual(canvas[(3, 0)], Color(0, 0, 128))
        self.assertEqual(canvas[(0, 1)], Color(0, 255, 0))
        self.assertEqual(canvas[(1, 1)], Color(255, 255, 0))
        self.assertEqual(canvas[(2, 1)], Color(128, 128, 128))
        self.assertEqual(canvas[(3, 1)], Color(128, 0, 128))
        self.assertEqual(canvas[(0, 2)], Color(0, 0, 0))
        self.assertEqual(canvas[(1, 2)], Color(0, 255, 255))
        self.assertEqual(canvas[(2, 2)], Color(255, 255, 255))
        self.assertEqual(canvas[(3, 2)], Color(0, 128, 128))
        self.assertEqual(canvas[(0, 3)], Color(128, 0, 0))
        self.assertEqual(canvas[(1, 3)], Color(0, 128, 0))
        self.assertEqual(canvas[(2, 3)], Color(128, 128, 0))
        self.assertEqual(canvas[(3, 3)], Color(192, 192, 192))

        canvas = WorkingCanvas(200, 200, Color.white())
        self.assertEqual(canvas.height, 200)
        self.assertEqual(canvas.width, 200)
        self.assertEqual(canvas.grid_height, 1)
        self.assertEqual(canvas.grid_width, 1)
        self.assertEqual(canvas[(0, 0)], Color(255, 255, 255))

        canvas = WorkingCanvas(201, 201, Color(10, 27, 3))
        self.assertEqual(canvas.height, 201)
        self.assertEqual(canvas.width, 201)
        self.assertEqual(canvas.grid_height, 2)
        self.assertEqual(canvas.grid_width, 2)
        self.assertEqual(canvas[(0, 0)], Color(10, 27, 3))


class TestBrush(unittest.TestCase):

    def test_draw_pixel(self):
        canvas = WorkingCanvas(2, 2, Color.white())
        brush = Brush(canvas, Color.black(), 1, 'brush', 'circle')
        changed = []
        brush.draw_pixel(0, 0, changed)
        self.assertEqual(canvas[(0, 0)], brush.color)
        self.assertEqual(canvas[(1, 0)], Color.white())
        self.assertEqual(canvas[(0, 1)], Color.white())
        self.assertEqual(canvas[(1, 1)], Color.white())
        self.assertEqual(changed[0], (Point(0, 0), Color.white()))

    def test_draw_point(self):
        canvas = WorkingCanvas(4, 4, Color.white())
        brush = Brush(canvas, Color.black(), 2, 'brush', 'circle')
        brush.draw_point(Point(2, 2))
        self.assertEqual(canvas[(1, 1)], brush.color)
        self.assertEqual(canvas[(2, 1)], brush.color)
        self.assertEqual(canvas[(1, 2)], brush.color)
        self.assertEqual(canvas[(2, 2)], brush.color)

    def test_draw_line(self):
        canvas = WorkingCanvas(4, 4, Color.white())
        brush = Brush(canvas, Color.black(), 2, 'brush', 'circle')
        brush.draw_line(Point(0, 0), Point(4, 4))
        self.assertEqual(canvas[(0, 0)], brush.color)
        self.assertEqual(canvas[(1, 1)], brush.color)
        self.assertEqual(canvas[(2, 2)], brush.color)
        self.assertEqual(canvas[(3, 3)], brush.color)

        canvas = WorkingCanvas(4, 4, Color.white())
        brush = Brush(canvas, Color.black(), 2, 'brush', 'circle')
        brush.draw_line(Point(0, 4), Point(4, 0))
        self.assertEqual(canvas[(3, 0)], brush.color)
        self.assertEqual(canvas[(2, 1)], brush.color)
        self.assertEqual(canvas[(1, 2)], brush.color)
        self.assertEqual(canvas[(0, 3)], brush.color)

        canvas = WorkingCanvas(3, 3, Color.white())
        brush = Brush(canvas, Color.black(), 1, 'brush', 'circle')
        brush.draw_line(Point(2, 0), Point(2, 3))
        self.assertEqual(canvas[(1, 0)], brush.color)
        self.assertEqual(canvas[(1, 1)], brush.color)
        self.assertEqual(canvas[(1, 2)], brush.color)

        canvas = WorkingCanvas(3, 3, Color.white())
        brush = Brush(canvas, Color.black(), 1, 'brush', 'circle')
        brush.draw_line(Point(0, 2), Point(3, 2))
        self.assertEqual(canvas[(0, 1)], brush.color)
        self.assertEqual(canvas[(1, 1)], brush.color)
        self.assertEqual(canvas[(2, 1)], brush.color)


class TestFilter(unittest.TestCase):

    def test_grayscale(self):
        canvas = WorkingCanvas(1, 1, Color(255, 0, 0))
        Filter.grayscale(canvas, None)
        self.assertTrue(canvas[(0, 0)].red == canvas[(0, 0)].green)
        self.assertTrue(canvas[(0, 0)].green == canvas[(0, 0)].blue)

    def test_negative(self):
        canvas = WorkingCanvas(1, 1, Color(255, 2, 56))
        Filter.negative(canvas, None)
        self.assertTrue(canvas[(0, 0)].red == 0)
        self.assertTrue(canvas[(0, 0)].green == 253)
        self.assertTrue(canvas[(0, 0)].blue == 199)


class TestTools(unittest.TestCase):

    def test_brightness(self):
        canvas = WorkingCanvas(1, 1, Color(255, 2, 56))
        Tools.brightness(canvas, 0, None)
        self.assertTrue(canvas[(0, 0)].red == 255)
        self.assertTrue(canvas[(0, 0)].green == 2)
        self.assertTrue(canvas[(0, 0)].blue == 56)

        canvas = WorkingCanvas(1, 1, Color(255, 2, 56))
        Tools.brightness(canvas, 20, None)
        self.assertTrue(canvas[(0, 0)].red == 255)
        self.assertTrue(canvas[(0, 0)].green == 22)
        self.assertTrue(canvas[(0, 0)].blue == 76)

        canvas = WorkingCanvas(1, 1, Color(0, 2, 56))
        Tools.brightness(canvas, -30, None)
        self.assertTrue(canvas[(0, 0)].red == 0)
        self.assertTrue(canvas[(0, 0)].green == 0)
        self.assertTrue(canvas[(0, 0)].blue == 26)

        canvas = WorkingCanvas(1, 1, Color(255, 2, 56))
        Tools.brightness(canvas, 256, None)
        self.assertTrue(canvas[(0, 0)].red == 255)
        self.assertTrue(canvas[(0, 0)].green == 255)
        self.assertTrue(canvas[(0, 0)].blue == 255)


class TestWorkSpace(unittest.TestCase):

    def test_init(self):
        workspace = WorkSpace(100, 200, 96, Color.black())
        self.assertEqual(workspace.file_path, None)
        self.assertEqual(type(workspace.brush), Brush)
        self.assertEqual(type(workspace.canvas), WorkingCanvas)
        self.assertEqual(type(workspace.image_info), ImageInfo)
        self.assertEqual(workspace.canvas.background_color, Color.black())
        self.assertEqual(workspace.canvas.height, 200)
        self.assertEqual(workspace.canvas.width, 100)


if __name__ == '__main__':
    unittest.main()
