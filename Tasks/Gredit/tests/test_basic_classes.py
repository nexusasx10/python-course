import unittest

from basics import Point, Color


class TestPoint(unittest.TestCase):

    def test_is_within(self):
        self.assertEqual(Point(0.5, 0.5).is_within(0, 1, 0, 1), True)
        self.assertEqual(Point(0, 0).is_within(0, 1, 0, 1), True)
        self.assertEqual(Point(1, 1).is_within(0, 1, 0, 1), True)
        self.assertEqual(Point(0, 1).is_within(0, 1, 0, 1), True)
        self.assertEqual(Point(1, 0).is_within(0, 1, 0, 1), True)
        self.assertEqual(Point(2, 2).is_within(0, 1, 0, 1), False)
        self.assertEqual(Point(0.5, 2).is_within(0, 1, 0, 1), False)
        self.assertEqual(Point(2, 0.5).is_within(0, 1, 0, 1), False)
        self.assertEqual(Point(-2, -2).is_within(0, 1, 0, 1), False)

    def test_shift(self):
        self.assertEqual(Point(0, 0).shift(0, 0), Point(0, 0))
        self.assertEqual(Point(0, 0).shift(1, 0), Point(1, 0))
        self.assertEqual(Point(0, 0).shift(0, 1), Point(0, 1))
        self.assertEqual(Point(0, 0).shift(1, 1), Point(1, 1))
        self.assertEqual(Point(0, 0).shift(-1, 0), Point(-1, 0))
        self.assertEqual(Point(0, 0).shift(0, -1), Point(0, -1))
        self.assertEqual(Point(0, 0).shift(-1, -1), Point(-1, -1))

    def test_to_tuple(self):
        self.assertEqual(type(Point(0, 0).to_tuple()), tuple)
        self.assertEqual(Point(10, -234).to_tuple(), (10, -234))


class TestColor(unittest.TestCase):

    def test_init(self):
        self.assertEqual(Color(hex_='#0c2238'), Color(12, 34, 56))
        self.assertEqual(Color(hex_='#ffc536'), Color(255, 197, 54))
        self.assertEqual(Color(-1, 1000000, 0), Color(0, 255, 0))
        self.assertEqual(Color(500, 1024, -80), Color(255, 255, 0))

    def test_to_hex(self):
        self.assertEqual(Color(87, 65, 43).to_hex(), '#57412b')
        self.assertEqual(Color(256, 11, -9).to_hex(), '#ff0b00')

    def test_constants(self):
        self.assertEqual(Color.black(), Color(0, 0, 0))
        self.assertEqual(Color.white(), Color(255, 255, 255))

    def test_to_tuple(self):
        self.assertEqual(type(Color(0, 0, 0).to_tuple()), tuple)
        self.assertEqual(Color(1, 2, 4).to_tuple(), (1, 2, 4))


if __name__ == '__main__':
    unittest.main()
