"""Библиотека базовых классов."""


class EditorError(Exception):
    """Базовый класс для всех програмных исключений."""

    def __init__(self, value, critical):
        self.value = value
        self.critical = critical
        super().__init__()


class FileOpenError(EditorError):
    """Ошибка при открытии файла."""


class FileSaveError(EditorError):
    """Ошибка при сохранении файла."""


class Point:
    """Представление точки."""

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        return self.x**3 + self.y

    def to_tuple(self):
        return self.x, self.y

    def is_within(self, x_from, x_to, y_from, y_to):
        """Проверяет находится ли точка в данных пределах."""
        return x_from <= self.x <= x_to and y_from <= self.y <= y_to

    def shift(self, offset_x, offset_y):
        """Возвращает точку, сдвинутую относительно исходной."""
        return Point(self.x + offset_x, self.y + offset_y)


class Color:
    """Представление цвета."""

    def __init__(self, red=None, green=None, blue=None, hex_=None):
        if hex_ is not None:
            self.red = int(hex_[1:3], base=16)
            self.green = int(hex_[3:5], base=16)
            self.blue = int(hex_[5:7], base=16)
        else:
            if red < 0:
                self.red = 0
            elif red > 255:
                self.red = 255
            else:
                self.red = red
            if green < 0:
                self.green = 0
            elif green > 255:
                self.green = 255
            else:
                self.green = green
            if blue < 0:
                self.blue = 0
            elif blue > 255:
                self.blue = 255
            else:
                self.blue = blue

    def __eq__(self, other):
        return self.red == other.red and \
               self.green == other.green and \
               self.blue == other.blue

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        return self.red**5 + self.green**2 + self.blue

    def to_tuple(self):
        return self.red, self.green, self.blue

    def to_hex(self):
        """Возвращает HEX-код цвета."""
        return '#{:0>2}{:0>2}{:0>2}'.format(
            *map(lambda c: hex(c)[2:], self.to_tuple())
        )
