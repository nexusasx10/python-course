"""Библиотека утилитарных классов."""
from time import clock

from basics import Color


class Timer:
    """Менеджер контекста, замеряющий время выполнения кода."""
    def __enter__(self):
        self._startTime = clock()

    def __exit__(self, type, value, traceback):
        print("Прошло: {:.8f} сек.".format(clock() - self._startTime))


class Colors:

    black = Color(0, 0, 0)
    white = Color(255, 255, 255)
