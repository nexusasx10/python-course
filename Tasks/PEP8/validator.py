# !/usr/bin/env python3
import argparse
import logging
from keyword import kwlist
from os.path import isdir, isfile, join
from os import walk
from collections import defaultdict

from modules.checkers import LineLengthChecker, IndentChecker, \
    StatementChecker, DeprecatedChecker
from modules.checkers import WhitespaceChecker, BlankLineChecker
from modules.issue import IssueCode
from modules.parser import Parser
from modules.readers import RegexReader
from modules.token_ import TokenType
from modules.utils.regex_utils import alt, maybe, star
from modules.views.console_view import ConsoleView


BITWISE_OPERATORS = {'&=?', '^=?', '~', '\|=?', '<<=?', '>>=?'}
ARITHMETIC_OPERATORS = {'\+=?', '-=?', '\*=?', '/=?', '//=?', '\*\*=?'}
OPERATORS = {'==?', '<=?', '!=', '<>', '>=?'}
DIGIT_PART = r'[0-9](_?[0-9])*'
FRACTION = r'\.' + DIGIT_PART
EXPONENT = '[eE][+-]?' + DIGIT_PART
POINT_FLOAT = alt(maybe(DIGIT_PART) + FRACTION, DIGIT_PART + '\.')
EXPONENT_FLOAT = alt(DIGIT_PART, POINT_FLOAT) + EXPONENT
FLOAT_NUMBER = alt(POINT_FLOAT, EXPONENT_FLOAT)
IMAGE_NUMBER = alt(FLOAT_NUMBER, DIGIT_PART) + '[jJ]'
STRING_PREFIX = alt(
    *'r u R U f F fr Fr fR FR rf rF Rf RF b B br Br bR BR rb rB Rb RB'.split()
)
SINGLE_QUOTE_STRING_ITEM = '[^\\\\\'\n]|(\\\\.)'
DOUBLE_QUOTE_STRING_ITEM = '[^\\\\"\n]|(\\\\.)'
DOCSTRING_ITEM = r'\\?(\n|.)'
SINGLE_QUOTE_STRING = '\'' + star(SINGLE_QUOTE_STRING_ITEM) + '\''
DOUBLE_QUOTE_STRING = '"' + star(DOUBLE_QUOTE_STRING_ITEM) + '"'
STRING = maybe(STRING_PREFIX) + alt(SINGLE_QUOTE_STRING, DOUBLE_QUOTE_STRING)
DOCSTRING = maybe(STRING_PREFIX) + alt(
    '\'\'\'' + star(DOCSTRING_ITEM) + '\'\'\'',
    '"""' + star(DOCSTRING_ITEM) + '"""'
)

DEFAULT_READERS = (
    RegexReader(*kwlist, types=TokenType.KEYWORD),
    RegexReader(r'([ \t]+)', types=TokenType.WHITESPACE),
    RegexReader(r'\n', types=TokenType.NEWLINE),
    RegexReader(r'#.*', types=TokenType.COMMENT),
    RegexReader(
        DOCSTRING,
        types=TokenType.STRING | TokenType.CONSTANT | TokenType.DOC
    ),
    RegexReader(STRING, types=TokenType.STRING | TokenType.CONSTANT),
    RegexReader(
        r'0[bB][01]+',
        types=TokenType.INT | TokenType.BIN | TokenType.CONSTANT
    ),
    RegexReader(
        r'0[oO][0-7]+',
        types=TokenType.INT | TokenType.OCT | TokenType.CONSTANT
    ),
    RegexReader(
        r'([1-9](_?[0-9])*)|0',
        types=TokenType.INT | TokenType.DEC | TokenType.CONSTANT
    ),
    RegexReader(
        r'0[xX][0-9a-fA-F]+',
        types=TokenType.INT | TokenType.HEX | TokenType.CONSTANT
    ),
    RegexReader(POINT_FLOAT, types=TokenType.FLOAT | TokenType.CONSTANT),
    RegexReader(EXPONENT_FLOAT, types=TokenType.FLOAT | TokenType.CONSTANT),
    RegexReader(r'@', types=TokenType.DECORATOR),
    RegexReader(r':', types=TokenType.COLON),
    RegexReader(r';', types=TokenType.SEMICOLON),
    RegexReader(r'\\', types=TokenType.WRAPPING),
    RegexReader(r',', types=TokenType.COMMA),
    RegexReader(r'\.', types=TokenType.DOT),
    RegexReader(r'->', types=TokenType.ANNOTATION),
    RegexReader(r'`', types=TokenType.BACKTICK),
    RegexReader(r'[\(\[\{]', types=TokenType.OPEN | TokenType.BRACKET),
    RegexReader(r'[\)\]\}]', types=TokenType.CLOSE | TokenType.BRACKET),
    RegexReader(r'[A-Za-z_][A-Za-z0-9_]*', types=TokenType.IDENTIFIER),
    RegexReader('%=?', types=TokenType.MODULO | TokenType.OPERATOR),
    RegexReader(
        *ARITHMETIC_OPERATORS,
        types=TokenType.ARITHMETIC | TokenType.OPERATOR
    ),
    RegexReader(
        *BITWISE_OPERATORS,
        types=TokenType.BITWISE | TokenType.OPERATOR
    ),
    RegexReader(*OPERATORS, types=TokenType.OPERATOR)
)

DEFAULT_CHECKERS = {
    IndentChecker(),
    WhitespaceChecker(),
    BlankLineChecker(),
    LineLengthChecker(),
    StatementChecker(),
    DeprecatedChecker(),
}

DEFAULT_CORRECTORS = {

}

DEFAULT_IGNORE = {
    IssueCode.E121,
    IssueCode.E123,
    IssueCode.E126,
    IssueCode.E133,
    IssueCode.E226,
    IssueCode.E241,
    IssueCode.E242,
    IssueCode.E704,
    IssueCode.W503,
    IssueCode.W504,
}


class FileTokenReader:
    """Читает токены из файла."""
    def __init__(self, parser, filename):
        self._parser = parser
        self._filename = filename

    def read(self):
        with open(self._filename, encoding='utf-8') as fd:
            for line_number, line in enumerate(fd):
                yield from self._parser.parse(line_number, line)


class Validator:

    def __init__(self, args, parser, checkers, correctors, ignore):
        self._args = args
        self._parser = parser
        self._checkers = checkers
        self._correctors = correctors
        self._ignore = ignore

    def validate(self, paths):
        # todo too complex
        result = defaultdict(list)
        file_paths = []
        for path in paths:
            if isdir(path):
                for root, _, files in walk(path):
                    files = filter(lambda f: f.endswith('.py'), files)
                    files = map(lambda f: join(root, f), files)
                    file_paths.extend(files)
            elif isfile(path):
                if path.endswith('.py'):
                    file_paths.append(path)
        for file_path in file_paths:

        return filter(lambda x: x[0] not in self._ignore, result)


    def check(self, file_path):
        try:
            token = None
            token_index = 0
            with open(file_path, encoding='utf-8') as fd:
                break_ = False
                for i, line in enumerate(fd):
                    for token in self._parser.parse(i, line):
                        if self._parser.error:
                            yield IssueCode.E902, token_index
                            break_ = True
                            break
                        for checker in self._checkers:
                            yield from checker.check(token)
                    if break_:
                        break
                for checker in self._checkers:  # todo нужно ли?
                    codes = checker.post()
                    if codes and token:
                        yield from (code, token_index for code in codes)
        except OSError:
            yield IssueCode.E902, None


def main():
    parser = argparse.ArgumentParser(description='Validate python code.')
    parser.add_argument(
        '-p', '--paths',
        help='list of paths to .py file or directory',
        nargs='*',
        default=['.']
    )
    parser.add_argument(
        '-i', '--ignore',
        help='list of issue codes to ignore',
        nargs='*',
        default=[]
    )
    parser.add_argument(
        '-f', '--fix',
        help='fix issues, if possible',
        action='store_true'
    )
    parser.add_argument(
        '-l', '--log',
        help='write logs to the file',
        action='store_true'
    )
    args = parser.parse_args()
    ignore = set()
    for issue in args.ignore:
        try:
            ignore.add(IssueCode[issue])
        except KeyError:
            logging.error('Wrong issue code: ' + issue)
            return
    if not ignore:
        ignore = DEFAULT_IGNORE
    validator = Validator(
        args,
        Parser(DEFAULT_READERS),
        DEFAULT_CHECKERS,
        DEFAULT_CORRECTORS,
        ignore
    )
    view = ConsoleView(args)
    view.show(validator.validate(args.paths))


if __name__ == '__main__':
    main()
