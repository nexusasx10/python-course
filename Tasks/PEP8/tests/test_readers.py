import unittest

from modules.token_ import Token, TokenType
from validator import RegexReader, STRING, DOCSTRING, POINT_FLOAT


class TestRegexReader(unittest.TestCase):

    def test_regex_reader(self):
        reader = RegexReader('text', types=1 | 2 | 4)
        token = reader.try_read('text', 0, 0, 0)
        self.assertEqual(token, Token(1 | 2 | 4, 'text', 0, 0, 0))
        token = reader.try_read('_text', 1, 1, 1)
        self.assertEqual(token, Token(1 | 2 | 4, 'text', 1, 1, 1))


class TestStringReader(unittest.TestCase):

    def setUp(self):
        self.reader = RegexReader(
            STRING,
            types=TokenType.STRING | TokenType.CONSTANT
        )

    def test_single_quote_string(self):
        token = self.reader.try_read('\'."az09-+\\n\'123', 0, 0, 0)
        self.assertEqual(token.text, '\'."az09-+\\n\'')

    def test_double_quote_string(self):
        token = self.reader.try_read('".\'az09-+\\n"123', 0, 0, 0)
        self.assertEqual(token.text, '".\'az09-+\\n"')


class TestDocstringReader(unittest.TestCase):

    def setUp(self):
        self.reader = RegexReader(
            DOCSTRING,
            types=TokenType.STRING | TokenType.CONSTANT | TokenType.DOC
        )

    def test_single_quote_docstring(self):
        token = self.reader.try_read('\'\'\'."""\'\'az09-+\\n\'\'\'123', 0, 0, 0)
        self.assertEqual(token.text, '\'\'\'."""\'\'az09-+\\n\'\'\'')

    def test_double_quote_docstring(self):
        token = self.reader.try_read('""".""\'\'\'az09-+\\n"""123', 0, 0, 0)
        self.assertEqual(token.text, '""".""\'\'\'az09-+\\n"""')

    def test_wrapped_docstring(self):
        token = self.reader.try_read('"""\n123\n456\n"""', 0, 0, 0)
        self.assertEqual(token.text, '"""\n123\n456\n"""')


class TestIdentifierReader(unittest.TestCase):

    def setUp(self):
        self.reader = RegexReader(
            r'[A-Za-z_][A-Za-z0-9_]*',
            types=TokenType.IDENTIFIER
        )

    def test_alpha(self):
        token = self.reader.try_read('foo', 0, 0, 0)
        self.assertEqual(token.text, 'foo')

    def test_number(self):
        token = self.reader.try_read('foo2', 0, 0, 0)
        self.assertEqual(token.text, 'foo2')
        token = self.reader.try_read('foo2bar', 0, 0, 0)
        self.assertEqual(token.text, 'foo2bar')

    def test_underscore(self):
        token = self.reader.try_read('_foo', 0, 0, 0)
        self.assertEqual(token.text, '_foo')
        token = self.reader.try_read('foo_bar', 0, 0, 0)
        self.assertEqual(token.text, 'foo_bar')
        token = self.reader.try_read('foo_', 0, 0, 0)
        self.assertEqual(token.text, 'foo_')

    def test_leading_number(self):
        token = self.reader.try_read('2foo', 0, 0, 0)
        self.assertIsNone(token)


class TestDecNumberReader(unittest.TestCase):

    def setUp(self):
        self.reader = RegexReader(
            r'([1-9](_?[0-9])*)|0',
            types=TokenType.INT | TokenType.DEC | TokenType.CONSTANT
        )

    def test_default(self):
        token = self.reader.try_read('1234567890', 0, 0, 0)
        self.assertEqual(token.text, '1234567890')

    def test_zero(self):
        token = self.reader.try_read('0', 0, 0, 0)
        self.assertEqual(token.text, '0')

    def test_underscore(self):
        token = self.reader.try_read('123_456_789', 0, 0, 0)
        self.assertEqual(token.text, '123_456_789')

    def test_leading_zero(self):
        token = self.reader.try_read('0123', 0, 0, 0)
        self.assertEqual(token.text, '0')


class TestHexNumberReader(unittest.TestCase):

    def setUp(self):
        self.reader = RegexReader(
            r'0[xX][0-9a-fA-F]+',
            types=TokenType.INT | TokenType.HEX | TokenType.CONSTANT
        )

    def test_default(self):
        token = self.reader.try_read('0x123456789abcdefABCDEF', 0, 0, 0)
        self.assertEqual(token.text, '0x123456789abcdefABCDEF')


class TestFloatNumberReader(unittest.TestCase):

    def setUp(self):
        self.reader = RegexReader(
            POINT_FLOAT,
            types=TokenType.FLOAT | TokenType.CONSTANT
        )

    def test_default(self):
        token = self.reader.try_read('12345.67890', 0, 0, 0)
        self.assertEqual(token.text, '12345.67890')

    def test_empty_first(self):
        token = self.reader.try_read('.1', 0, 0, 0)
        self.assertEqual(token.text, '.1')

    def test_empty_second(self):
        token = self.reader.try_read('0.', 0, 0, 0)
        self.assertEqual(token.text, '0.')

    def test_underscore(self):
        token = self.reader.try_read('123_4.6_789', 0, 0, 0)
        self.assertEqual(token.text, '123_4.6_789')
