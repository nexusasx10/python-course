from unittest import TestCase

from _temp import DeprecatedChecker
from modules.base_checker import _FlagWriteSpace, _FlagReadSpace
from modules.checkers import IndentChecker, WhitespaceChecker, \
    BlankLineChecker, ImportChecker, LineLengthChecker, StatementChecker
from modules.issue import IssueCode
from modules.parser import Parser
from validator import DEFAULT_READERS


class TestCheckerAPI(TestCase):

    def setUp(self):
        self.flags = {
            'first': [1, 2, 3],
            'second': [4, 5, 6]
        }

    def test_flags_read(self):
        flags = _FlagReadSpace(self.flags, 1)
        self.assertEqual(flags[-1].first, 1)
        self.assertEqual(flags[-1].second, 4)
        self.assertEqual(flags.first, 2)
        self.assertEqual(flags.first, 5)
        self.assertEqual(flags[1].first, 3)
        self.assertEqual(flags[1].first, 6)

    def test_flags_write(self):
        flags = _FlagWriteSpace(self.flags, 1)
        flags.first = 0
        self.assertEqual(flags[-1].first, 1)
        self.assertEqual(flags[-1].second, 4)
        self.assertEqual(flags.first, 0)
        self.assertEqual(flags.first, 5)
        self.assertEqual(flags[1].first, 3)
        self.assertEqual(flags[1].first, 6)


class TestChecker(TestCase):
    # todo тесты ка на корректные срабатываания, так и на отсутствие ложных

    def check(self, *lines):
        result = []
        self.parser = Parser(DEFAULT_READERS)
        for i, line in enumerate(lines):
            for token in self.parser.parse(i, line):
                codes = self.checker.before(token)
                if codes:
                    result.extend(codes)
                codes = self.checker.check(token)
                if codes:
                    result.extend(codes)
                codes = self.checker.after(token)
                if codes:
                    result.extend(codes)
        return list(map(lambda x: x[0], result))


class TestIndentChecker(TestChecker):

    def setUp(self):
        self.checker = IndentChecker()

    def test_E101(self):
        result = self.check('def a():\n', '    print()\n', '\tprint()')
        self.assertIn(IssueCode.E101, result)

    def test_E111(self):
        result = self.check('def a():\n', '  print()')
        self.assertIn(IssueCode.E111, result)

    def test_E112(self):
        result = self.check('def a():\n', 'print()')
        self.assertIn(IssueCode.E112, result)

    def test_E113(self):
        result = self.check('print()\n', '    print()\n')
        self.assertIn(IssueCode.E113, result)

    def test_E114(self):
        result = self.check('def a():\n', ' # print()\n', '    print()')
        self.assertIn(IssueCode.E114, result)

    def test_E115(self):
        result = self.check('def a():\n', '# print()\n', '    print()')
        self.assertIn(IssueCode.E115, result)

    def test_E116(self):
        result = self.check('print()\n', '    # print()\n', '    print()')
        self.assertIn(IssueCode.E116, result)


class TestWhitespaceChecker(TestChecker):

    def setUp(self):
        self.checker = WhitespaceChecker()

    def test_E201(self):
        result = self.check('print( 2)')
        self.assertIn(IssueCode.E201, result)
        result = self.check('[ 2]')
        self.assertIn(IssueCode.E201, result)
        result = self.check('{ 2}')
        self.assertIn(IssueCode.E201, result)

    def test_E202(self):
        result = self.check('print(2 )')
        self.assertIn(IssueCode.E202, result)
        result = self.check('[2 ]')
        self.assertIn(IssueCode.E202, result)
        result = self.check('{2 }')
        self.assertIn(IssueCode.E202, result)

    def test_E203(self):
        result = self.check('a = 2 , 3')
        self.assertIn(IssueCode.E203, result)
        result = self.check('if a : do()')
        self.assertIn(IssueCode.E203, result)
        result = self.check('print() ;')
        self.assertIn(IssueCode.E203, result)

    def test_E221(self):
        result = self.check('3  + 2')
        self.assertIn(IssueCode.E221, result)

    def test_E222(self):
        result = self.check('3 +  2')
        self.assertIn(IssueCode.E222, result)

    def test_E223(self):
        result = self.check('3\t+ 2')
        self.assertIn(IssueCode.E223, result)

    def test_E224(self):
        result = self.check('3 +\t2')
        self.assertIn(IssueCode.E224, result)

    def test_E225(self):
        result = self.check('3>2')
        self.assertIn(IssueCode.E225, result)

    def test_E226(self):
        result = self.check('3+2')
        self.assertIn(IssueCode.E226, result)

    def test_E227(self):
        result = self.check('3|2')
        self.assertIn(IssueCode.E227, result)

    def test_E228(self):
        result = self.check('3%2')
        self.assertIn(IssueCode.E228, result)

    def test_E231(self):
        result = self.check('a = 3,2')
        self.assertIn(IssueCode.E231, result)

    def test_E242(self):
        result = self.check('a = 3,\t3')
        self.assertIn(IssueCode.E242, result)

    def test_E251(self):
        result = self.check('print(3, sep = 4')
        self.assertIn(IssueCode.E251, result)

    def test_E261(self):
        result = self.check('print(1)# comment')
        self.assertIn(IssueCode.E261, result)

    def test_E262(self):
        result = self.check('print(1) #comment')
        self.assertIn(IssueCode.E262, result)

    def test_E265(self):
        result = self.check('\n', '#comment')
        self.assertIn(IssueCode.E265, result)

    def test_E266(self):
        result = self.check('\n', '###comment')
        self.assertIn(IssueCode.E266, result)

    def test_E271(self):
        result = self.check('import   logging')
        self.assertIn(IssueCode.E271, result)

    def test_E272(self):
        result = self.check('True   and False')
        self.assertIn(IssueCode.E272, result)

    def test_E273(self):
        result = self.check('if True\t:')
        self.assertIn(IssueCode.E273, result)

    def test_E274(self):
        result = self.check('if\tTrue:')
        self.assertIn(IssueCode.E274, result)


class TestBlankLineChecker(TestChecker):

    def setUp(self):
        self.checker = BlankLineChecker()

    def test_E302(self):
        result = self.check('print(1)', 'def a(): pass')
        self.assertIn(IssueCode.E302, result)

    def test_E303(self):
        result = self.check('\n', '\n', '\n', 'def a(): pass')
        self.assertIn(IssueCode.E303, result)

    def test_E304(self):
        result = self.check('@lru_cache(3)\n', '\n', 'def a(): pass')
        self.assertIn(IssueCode.E304, result)


class TestImportChecker(TestChecker):

    def setUp(self):
        self.checker = ImportChecker()

    def test_E401(self):
        result = self.check('import a, b, c')
        self.assertIn(IssueCode.E401, result)


class TestLineLengthChecker(TestChecker):

    def setUp(self):
        self.checker = LineLengthChecker()

    def test_E501(self):
        result = self.check('a' * 80 + '\n')
        self.assertIn(IssueCode.E501, result)


class TestStatementChecker(TestChecker):

    def setUp(self):
        self.checker = StatementChecker()

    def test_E701(self):
        result = self.check('if a: b()')
        self.assertIn(IssueCode.E701, result)

    def test_E702(self):
        result = self.check('print(1); print(2)')
        self.assertIn(IssueCode.E702, result)

    def test_E703(self):
        result = self.check('print(1);')
        self.assertIn(IssueCode.E703, result)

    def test_E704(self):
        result = self.check('def a(): print(1)')
        self.assertIn(IssueCode.E704, result)

    def test_E741(self):
        result = self.check('I = 0')
        self.assertIn(IssueCode.E741, result)


class TestDeprecatedChecker(TestChecker):

    def setUp(self):
        self.checker = DeprecatedChecker()

    def test_W603(self):
        result = self.check('if a <> b: return')
        self.assertIn(IssueCode.W603, result)

    def test_W604(self):
        result = self.check('`some_object`')
        self.assertIn(IssueCode.W604, result)
