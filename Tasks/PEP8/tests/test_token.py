import unittest

from modules.token_ import Token, TokenType


class TestTypes(unittest.TestCase):

    def setUp(self):
        self.token = Token(
            TokenType.COLON | TokenType.IDENTIFIER, None, None, None, None
        )

    def test_correct_types(self):
        self.assertTrue(self.token[TokenType.COLON | TokenType.IDENTIFIER])
        self.assertTrue(self.token[TokenType.COLON])
        self.assertTrue(self.token[TokenType.IDENTIFIER])

    def test_wrong_types(self):
        self.assertFalse(self.token[TokenType.OPERATOR])
        self.assertFalse(self.token[TokenType.COLON | TokenType.OPERATOR])
        self.assertFalse(self.token[TokenType.OPERATOR | TokenType.IDENTIFIER])
        self.assertFalse(
            self.token[
                TokenType.OPERATOR | TokenType.IDENTIFIER | TokenType.COLON
            ]
        )
