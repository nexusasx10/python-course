from enum import IntFlag, auto
from functools import total_ordering


class TokenType(IntFlag):

    def _generate_next_value_(name, start, count, last_values):
        return 2 ** count

    NEWLINE = auto()
    WHITESPACE = auto()
    KEYWORD = auto()
    COMMENT = auto()
    DOC = auto()
    CONSTANT = auto()
    STRING = auto()
    INT = auto()
    BIN = auto()
    OCT = auto()
    DEC = auto()
    HEX = auto()
    FLOAT = auto()
    DECORATOR = auto()
    COLON = auto()
    SEMICOLON = auto()
    WRAPPING = auto()
    COMMA = auto()
    DOT = auto()
    ANNOTATION = auto()
    BACKTICK = auto()
    BRACKET = auto()
    OPEN = auto()
    CLOSE = auto()
    IDENTIFIER = auto()
    OPERATOR = auto()
    MODULO = auto()
    ARITHMETIC = auto()
    BITWISE = auto()


@total_ordering
class Token:

    def __init__(self, types, text, start, line, line_index, global_index):
        self.types = types
        self.text = text
        self.start = start
        self.line = line
        self.line_index = line_index
        self.global_index = global_index

    def __getitem__(self, types):
        return ~(~types | self.types) == 0

    def __len__(self):
        return len(self.text)

    def __eq__(self, other):
        return self.types == other.types and self.text == other.text

    def __hash__(self):
        return hash(self.types) ^ (hash(self.text) * 17)

    def __lt__(self, other):
        return len(self) < len(other)

    def __str__(self):
        return '<[{}] : {} : {} : "{}">'.format(
            self.types,
            self.line,
            self.number,
            self.start,
            self.text.replace('\n', '\\n')
        )
