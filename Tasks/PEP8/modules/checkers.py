from collections import Iterable

from modules.base_checker import BaseChecker
from modules.issue import IssueCode


BRACKETS = {'(': ')', '[': ']', '{': '}'}


class IndentChecker(BaseChecker):

    def __init__(self):
        super().__init__()
        self.wrapped = False
        self.indent_type = None
        self.current_line_indent = 0
        self.last_line_indent = None
        self.newline = True
        self.extra_indent_expected = False
        self.delayed_errors = []
        self.was_tab_report = False

    def after(self, token, flags):
        if self.newline:
            self.newline = False
        if token['newline']:
            self.newline = True
            self.delayed_errors = []
        elif token['wrapping']:
            self.wrapped = True

    def indent_type_rule(self, token):
        if self.newline and token['whitespace']:
            indent_type = token.text[0]
            if not self.indent_type:
                self.indent_type = indent_type
            elif len(set(token.text)) != 1 or indent_type != self.indent_type:
                yield IssueCode.E101

    def indent_length_rule(self, token):
        if self.newline and token['whitespace']:
            if len(token) % 4 != 0:
                self.delayed_errors.append(IssueCode.E111)

    def indent_expectation_rule(self, token):
        if self.newline:
            self.last_line_indent = self.current_line_indent
            if token['whitespace']:
                self.current_line_indent = len(token)
            if self.extra_indent_expected:
                if not self.wrapped:
                    if self.current_line_indent <= self.last_line_indent:
                        self.delayed_errors.append(IssueCode.E112)
                else:
                    self.wrapped = False
            else:
                if not self.wrapped:
                    if self.current_line_indent > self.last_line_indent:
                        self.delayed_errors.append(IssueCode.E113)
                else:
                    self.wrapped = False
        if token['colon']:
            self.extra_indent_expected = True  # todo а если однострочный?
        elif token['comment']:
            if IssueCode.E111 in self.delayed_errors:
                yield IssueCode.E114
            if IssueCode.E112 in self.delayed_errors:
                yield IssueCode.E115
            if IssueCode.E113 in self.delayed_errors:
                yield IssueCode.E116
            self.delayed_errors = []
        elif not token['whitespace'] and not token['newline']:
            # сбрасываем, когда код в той же строке
            self.extra_indent_expected = False
            # а новая строка? todo
            if self.delayed_errors:
                if IssueCode.E112 in self.delayed_errors:
                    yield IssueCode.E901
                yield from self.delayed_errors
                self.delayed_errors = []

    def tab_rule(self, token):
        if not self.was_tab_report:  # сообщить достаточно один раз
            if self.indent_type == '\t':
                yield IssueCode.W191
                self.was_tab_report = True


class WhitespaceChecker(BaseChecker):

    def __init__(self):
        self.prev_token = None
        self.bracket_stack = []
        self.token_number = 0  # номер токена в строке

    def post(self):
        if self.prev_token['whitespace']:
            yield IssueCode.W293

    def before(self, token):
        if token['open']:
            self.bracket_stack.append(token.text)

    def after(self, token):
        self.token_number += 1
        if token['newline']:
            self.token_number = 0
        elif token['close']:
            if len(self.bracket_stack) == 0:
                yield IssueCode.E901
            elif BRACKETS[self.bracket_stack.pop()] != token.text:
                yield IssueCode.E901
        self.prev_token = token

    def whitespace_before_rule(self, token):
        if self.prev_token and self.prev_token['whitespace']:
            if token['close']:
                yield IssueCode.E202
            elif token['comma'] or token['colon'] or token['semicolon']:
                yield IssueCode.E203

    def whitespace_after_rule(self, token):
        if token['whitespace']:
            if self.prev_token and self.prev_token['open']:
                yield IssueCode.E201

    def multiple_spaces_before_rule(self, token):
        if self.token_number == 1:
            return
        elif self.prev_token and self.prev_token['whitespace']:
            if len(self.prev_token) > 1:
                if token['operator']:
                    yield IssueCode.E221
                elif token['comma']:
                    yield IssueCode.E241
                elif token['keyword']:
                    yield IssueCode.E272

    def multiple_spaces_after_rule(self, token):
        if self.prev_token and token['whitespace'] and len(token) > 1:
            if self.prev_token['operator']:
                yield IssueCode.E222
            elif self.prev_token['keyword']:
                yield IssueCode.E271

    def tab_before_rule(self, token):
        if self.token_number == 1:
            return
        if self.prev_token and self.prev_token['whitespace']:
            if self.prev_token.text[-1] == '\t':
                if token['operator']:
                    yield IssueCode.E223
                elif token['keyword']:
                    yield IssueCode.E274

    def tab_after_rule(self, token):
        if token['whitespace']:
            if self.prev_token and token.text[-1] == '\t':
                if self.prev_token['operator']:
                    yield IssueCode.E224
                elif self.prev_token['comma']:
                    yield IssueCode.E242
                elif self.prev_token['keyword']:
                    yield IssueCode.E273

    def missing_whitespace_before_rule(self, token):
        if self.prev_token and not self.prev_token['whitespace']:
            if token['arithmetic'] and not self.prev_token['open']:
                yield IssueCode.E226
            elif token['bitwise']:
                yield IssueCode.E227
            elif token['modulo']:
                yield IssueCode.E228
            elif token['operator']:
                if token.text == '=' and '(' in self.bracket_stack:
                    return
                yield IssueCode.E225

    def missing_whitespace_after_rule(self, token):
        if self.prev_token and not token['whitespace']:
            if self.prev_token['arithmetic']:
                yield IssueCode.E226
            elif self.prev_token['bitwise']:
                yield IssueCode.E227
            elif self.prev_token['modulo']:
                yield IssueCode.E228
            elif self.prev_token['operator']:
                if self.prev_token.text == '=' and '(' in self.bracket_stack:
                    return
                yield IssueCode.E225
            elif self.prev_token['comma'] and not token['newline']:
                yield IssueCode.E231
            elif self.prev_token['colon'] or self.prev_token['semicolon']:
                if not token['newline'] and '[' not in self.bracket_stack:
                    yield IssueCode.E231

    def comment_rule(self, token):
        if token['comment']:
            if self.token_number == 0 or (
                    self.token_number == 1 and self.prev_token['whitespace']
            ):
                if not token.text.startswith('# '):
                    yield IssueCode.E265
                if token.text.startswith('##'):
                    yield IssueCode.E266
            else:
                if self.prev_token and self.prev_token['whitespace']:
                    if len(self.prev_token) < 2:
                        yield IssueCode.E261
                else:
                    yield IssueCode.E261
                if not token.text.startswith('# ') and token.text != '#':
                    yield IssueCode.E262

    def params_rule(self, token):
        if self.prev_token and '(' in self.bracket_stack:
            if token.text == '=' and self.prev_token['whitespace']:
                yield IssueCode.E251
            elif self.prev_token.text == '=' and token['whitespace']:
                yield IssueCode.E251

    def trailing_whitespace_rule(self, token):
        if token['newline']:
            if self.prev_token and self.prev_token['whitespace']:
                if self.token_number == 1:
                    yield IssueCode.W293
                else:
                    yield IssueCode.W291


class BlankLineChecker(BaseChecker):

    def __init__(self):
        super().__init__()
        self.blank_line_counter = 0
        self.new_line = True
        self.blank_line = True
        self.current_line_indent = 0
        self.decorated = False

    def post(self):
        if not(self.new_line and self.blank_line):
            yield IssueCode.W391

    def before(self, token):
        if self.new_line:
            self.new_line = False
            if token['whitespace']:
                self.current_line_indent = len(token)
            else:
                self.current_line_indent = 0

    def after(self, token):
        if token['newline']:
            self.new_line = True
            if self.blank_line:
                self.blank_line_counter += 1
            else:
                self.blank_line_counter = 0
            self.blank_line = True
        else:
            self.blank_line = False

    def expected_blank_lines_rule(self, token):
        if token.text in ('class', 'def') or token['decorator']:
            if self.decorated:
                self.decorated = False
                if self.blank_line_counter > 0:
                    yield IssueCode.E304
                return
            elif token['decorator']:
                self.decorated = True
            if self.current_line_indent > 0:
                if self.blank_line_counter == 0:
                    yield IssueCode.E306
                elif self.blank_line_counter > 1:
                    yield IssueCode.E303
            else:
                if self.blank_line_counter < 2:
                    yield IssueCode.E302
                elif self.blank_line_counter > 2:
                    yield IssueCode.E303


class ImportChecker(BaseChecker):

    def __init__(self):
        self.have_import = False
        self.have_from = False
        self.non_import_code_stated = False
        self.multiple_import_reported = False

    def after(self, token):
        if token['newline']:
            self.have_import = False
            self.multiple_import_reported = False

    def multiple_import_rule(self, token):
        if token['comma'] and self.have_import and not self.have_from:
            if not self.multiple_import_reported:
                yield IssueCode.E401
                self.multiple_import_reported = True
        if not self.have_import:
            self.have_import = (token.text == 'import')
        if not self.have_from:
            self.have_from = (token.text == 'from')

    def import_not_at_top_rule(self, token):
        # todo а что вообще с переносами? \
        if token['newline']:
            self.non_import_code_stated = self.have_import
        elif token.text == 'import' and self.non_import_code_stated:
            yield IssueCode.E402


class LineLengthChecker(BaseChecker):

    def __init__(self):
        self.current_line_length = 0
        self.bracket_stack = []

    def before(self, token):
        if token['open']:
            self.bracket_stack.append(token)
        elif token['close']:
            if len(self.bracket_stack) > 0:
                self.bracket_stack.pop()

    def line_length_rule(self, token):
        if token['newline']:
            if self.current_line_length > 79:
                yield IssueCode.E501
            self.current_line_length = 0
        else:
            self.current_line_length += len(token)

    def next_rule(self, token):
        if token['wrapping']:
            if len(self.bracket_stack) > 0:
                yield IssueCode.E502


class StatementChecker(BaseChecker):

    def __init__(self):
        self.bracket_stack = []
        self.prev_token = None
        self.function_definition = 0
        self.newline_expected = None
        self.definition_indent = 0
        self.first_token = True
        self.assignment = False

    def before(self, token):
        if token['open']:
            self.bracket_stack.append(token.text)
        elif token['newline']:
            self.newline_expected = None
        if self.first_token:
            self.first_token = False
            if token['whitespace']:
                if len(token) < self.definition_indent:
                    self.function_definition -= 1
            else:
                if self.definition_indent > 0:
                    self.function_definition -= 1

    def after(self, token):
        if token['close']:
            if len(self.bracket_stack) != 0:
                self.bracket_stack.pop()
        elif token['newline']:
            self.first_token = True
        self.prev_token = token

    def multiple_statements_rule(self, token):
        if token.text == 'def':
            self.function_definition += 1
            if self.prev_token and self.prev_token['whitespace']:
                self.definition_indent = len(self.prev_token)
        elif token['colon']:
            self.newline_expected = 'colon'
            return
        elif token['semicolon']:
            self.newline_expected = 'semicolon'
            yield IssueCode.E703
            return
        if not token['comment'] and not token['whitespace']:
            if self.newline_expected == 'colon':
                if '[' not in self.bracket_stack:
                    if self.function_definition:
                        yield IssueCode.E704
                    else:
                        yield IssueCode.E701
                else:
                    self.newline_expected = None
            elif self.newline_expected == 'semicolon':
                yield IssueCode.E702

    def lambda_assignment_rule(self, token):
        if token.text == '=':
            self.assignment = True
        if self.assignment:
            if token.text == 'lambda':
                yield IssueCode.E731
            if not token['whitespace']:
                self.assignment = False

    def bad_identifiers_rule(self, token):
        # todo допилить под функции и классы
        if token['identifier']:
            if token.text in ('l', 'O', 'I'):
                yield IssueCode.E741


class DeprecatedChecker(BaseChecker):  # todo Deprecation

    def deprecated_rule(self, token):
        if token.text == '<>':
            yield IssueCode.W603
        if token['backtick']:
            yield IssueCode.W604
