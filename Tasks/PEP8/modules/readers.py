import re

from modules.token_ import Token
from modules.utils.regex_utils import alt


class BaseReader:

    def try_read(self, text, line_number, token_number, start) -> Token:
        pass


class RegexReader(BaseReader):
    """Пытается считать токен по регулярному выражению."""
    def __init__(self, *patterns, types):
        self.regex = re.compile(alt(*patterns))
        self.types = types

    def try_read(self, text, line_number, token_number, start) -> Token:
        match = self.regex.match(text, pos=start)
        if match:
            return Token(
                self.types, match.group(0), line_number, token_number, start
            )
