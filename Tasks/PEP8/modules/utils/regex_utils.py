

def wrap(pattern):
    return '(' + pattern + ')'


def star(pattern):
    return wrap(pattern) + '*'


def maybe(pattern):
    return wrap(pattern) + '?'


def concat(*patterns):
    return ''.join(patterns)


def alt(*patterns):
    return '|'.join(map(wrap, patterns))
