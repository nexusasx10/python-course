from typing import Iterable

from modules.readers import BaseReader


class Parser:

    def __init__(self, readers: Iterable[BaseReader]):
        self.readers = readers
        self.error = False

    def parse(self, line_number, line):
        pointer = 0
        token_number = 0
        while pointer < len(line):
            token = None
            next_pointer = pointer
            for reader in self.readers:
                maybe_token = reader.try_read(
                    line, line_number, token_number, pointer
                )
                if maybe_token:
                    if not token or len(token) < len(maybe_token):
                        token = maybe_token
                        next_pointer = max(pointer + len(token), next_pointer)
            if token:
                yield token
                pointer = next_pointer
            else:
                self.error = True
                break
