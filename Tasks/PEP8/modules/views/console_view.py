import logging
import sys


class ConsoleView:

    def __init__(self, args):
        self.have_issues = False
        handlers = [logging.StreamHandler(sys.stdout)]
        if args.log:
            handlers.append(logging.FileHandler('validator_result.log'))
        logging.basicConfig(
            handlers=handlers,
            level=logging.INFO,
            format='%(message)s',)

    def show(self, issues):
        for file in issues:
            if issues[file]:
                self.have_issues = True
            for issue in issues[file]:
                if issue[1]:
                    logging.info(
                        '{}:{}:{}: {}'.format(
                            file,
                            issue[1].line + 1,
                            issue[1].start + 1,
                            issue[0]
                        )
                    )
                else:
                    logging.info('    {}'.format(issue[0]))
            logging.info('')
        if not self.have_issues:
            logging.info('No issues')
