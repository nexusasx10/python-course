from collections import defaultdict, Iterable


def rule_range(start, stop):
    def _rule_range(func):
        func.rule_range = start, stop
        func.rule_start = start
        func.rule_stop = stop
        return func
    return _rule_range


class _TokenSpace:

    def __init__(self, tokens, current, start, stop):
        self._tokens = tokens
        self._current = current
        self._start = start
        self._stop = stop

    def __getitem__(self, index):
        if self._start <= index <= self._stop:
            return self._tokens[self._current + index]
        else:
            raise IndexError('Index out of range')


class _FlagReadSpace:

    def __init__(self, flags, current):
        self._flags = flags
        self._current = current

    def __getitem__(self, index):
        new = _FlagReadSpace(self._flags, self._current)
        new._current += index
        return new

    def __getattr__(self, key):
        return self._flags[key][self._current]


class _FlagWriteSpace(_FlagReadSpace):

    def __setattr__(self, key, value):
        if key in ('_flags', '_current'):
            self.__dict__[key] = value
            return
        self._flags[key][self._current] = value


class BaseChecker:

    def __init__(self):
        self._flags = defaultdict(list)

    @property
    def _rules(self):
        return [
            v
            for k, v in self.__class__.__dict__.items()
            if k.endswith('_rule') and callable(v)
        ]

    def before(self, tokens, flags) -> Iterable:
        pass

    def after(self, tokens, flags) -> Iterable:
        pass

    def check(self, tokens):
        for i, token in enumerate(tokens):
            codes = self.before(token, _FlagWriteSpace(self._flags, i))
            if codes:
                yield from ((code, token) for code in codes)
            for rule in self._rules:
                if i < -rule.rule_start + rule.rule_stop:
                    continue
                codes = rule(
                    self,
                    _TokenSpace(tokens, i - rule.rule_stop, *rule.rule_range),
                    _FlagReadSpace(self._flags, i - rule.rule_stop)
                )
                if codes:
                    yield from ((code, token) for code in codes)
            codes = self.after(token, _FlagWriteSpace(self._flags, i))
            if codes:
                yield from ((code, token) for code in codes)
